# Jarvis Dashboard #

## Scripts


### `npm start` 
Runs the app in the development mode. (http://localhost:3000)
### `npm run build` 
Builds the app for production to the `build` folder.
### `npm run eject` 
https://create-react-app.dev/docs/available-scripts/#npm-run-eject

## Project structure

```txt
src/
    apis/           Data fetchers for all data sources (Coingecko, subgraphs, ...)
    components/     React components
    data/           Major constants & some explanation strings displayed on UI
    state/          Redux state
        slices/     Data slices of the state (Redux Toolkit)
        ... middlewares and store ...
    utils/
```
<br>

## Data sources

### Coingecko API ###

The Coingecko API is used to fetch the JRT marketcap.

### Sushiswap community subgraph ###

https://thegraph.com/explorer/subgraph/croco-finance/sushiswap

### UniswapV2 subgraph ###

https://thegraph.com/explorer/subgraph/uniswap/uniswap-v2


### Balancer subgraph ###

https://thegraph.com/explorer/subgraph/balancer-labs/balancer

### Synthereum subgraph ###

https://--url--

This subgraph is used to retrieve data about derivatives & trading on Synthereum.

### Synthereum holder tracking subgraph ###

https://--url--

This subgraph is used to retrieve the number of holders for JRT & synthetic tokens.


### Blocklytics subgraph ###

https://thegraph.com/explorer/subgraph/blocklytics/ethereum-blocks

This subgraph is used to retrieve blocks by timestamps. (used by https://uniswap.info)

### Chainlink community subgraph ###

https://thegraph.com/explorer/subgraph/openpredict/chainlink-prices-subgraph

This subraph uses a deprecated API and is a community subgraph (not official)
The subgraph is used to fetch the prices of the derivatives.

