import Web3 from 'web3';
import type { Contract } from 'web3-eth-contract';
import { Unit } from 'web3-utils';
import { splitAsyncOperation } from '../utils';
import { ERC20_ABI_READONLY } from './erc20-abi-read-only';

//NOT USED
export const web3 = new Web3();

export type { Contract } from 'web3-eth-contract';
export type { AbiItem } from 'web3-utils';

export async function getEthBalance(unit: Unit = 'wei', address: string){
    let value = await web3.eth.getBalance(address);
    value = web3.utils.fromWei(value, unit)
    console.debug('balance =', value, unit);
    return value;
}

export async function getERC20Balance(tokenAddress: string, address: string){
    let contract = new web3.eth.Contract(ERC20_ABI_READONLY, tokenAddress);
    
    //const decimals = await contract.methods.decimals().call();
    const name = await contract.methods.name().call();
    const decimals = await contract.methods.decimals().call();
    const balance = await contract.methods.balanceOf(address).call();

    return {
        name,
        decimals,
        balance
    }
}


export const {
    utils: {isAddress}
} = web3;


export function assertIsAddress(address: any): asserts address is string {
    if(typeof address !== 'string' || ! isAddress(address)) {
        throw new Error(`Not an address ${address}`);
    }
}


/**
 * @param {string} address
 * @returns the address in lowercase (removed checksum) and removed starting / trailing whitespaces
 */
export function cleanAddressAndRemoveChecksum(address: string){
    return address.toLowerCase().trim();
}


export async function makeBatchedCallsAtBlocks(contract: Contract, {signature, blocks}: {signature: string, blocks: number[]}): Promise<any[]>{

    /*
        MAKING TOO MANY REQUESTS IN A SAME BATCH CAN SOMETIMES PRODUCE ERRORS ON THE WEB3 PROVIDER SIDE:
        AVOID INCREASING
    */
    const MAX_REQ_IN_BATCH = 100;

    if(blocks.length === 0) return [];
    
    if(blocks.length > MAX_REQ_IN_BATCH){
        return splitAsyncOperation(MAX_REQ_IN_BATCH, blocks, (_blocks) => makeBatchedCallsAtBlocks(contract, {signature, blocks: _blocks}));
    }

    const methodSignatureHex = web3.eth.abi.encodeFunctionSignature(signature).padEnd(34, '0');
    const contractData = methodSignatureHex + contract.options.address.substring(2);

    
    let batch = new web3.BatchRequest();

    const resultCount = blocks.length;
    let remainingResultCount = resultCount;
    const results = new Array<any>(resultCount);

    let i = -1;

    return await new Promise((resolve, reject) => {


        setTimeout(() => reject('timeout'), 15_000)

        for(let block of blocks){
            i++;

            const blockHex = web3.utils.numberToHex(block);
            let req = {
                method: "eth_call",
                params: [
                    {
                        "to": (contract as Contract).options.address,
                        "data": contractData
                    },
                    blockHex
                ],
                i: i,
                callback(err: any, res: any){
                    if(err){
                        reject(err);
                    }
                    results[this.i] = res;
                    remainingResultCount --;
                    if(remainingResultCount === 0){
                        resolve(results);
                    }
                }
            };
            
            batch.add(req as any);
        }
    
        batch.execute() as any;
    })
}