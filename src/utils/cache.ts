const PREFIX = `jarvis/state/`;

const cacheObj = {


    set<T = any>(key: string, value: T) {
      return localStorage.setItem(PREFIX + key, JSON.stringify(value));
    },
  
    get<T = any>(key: string) {
      return JSON.parse(localStorage.getItem(PREFIX + key)!) as T | null;
    },

    has(key: string): boolean {
      return localStorage.getItem(PREFIX + key) !== null;
    },
  };
  
export const cache = typeof window === 'undefined' ? null : cacheObj;
  