export function timedOutFetch(args: {api: string, method?: 'POST' | 'GET', body?: any, timeout: number}){

    args.method  = args.method || 'GET';

    let signal = {};
    let timeout_handle: any = undefined;
    if(typeof window.AbortController === 'function'){
        const controller = new AbortController();
        
        timeout_handle = setTimeout(() => {
            controller.abort();
        }, args.timeout);
    
        signal = {signal: controller.signal}
    }      
    
    return fetch(args.api, {
        method: args.method,
        ... (args.body !== undefined) ? {
            body: args.body
        } : {},
        ...signal
    }).then(r => {
        if(timeout_handle) clearTimeout(timeout_handle);
        return r;
    });
    
}