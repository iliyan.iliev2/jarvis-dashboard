import { styled } from '@jarvis-network/ui';
import React from 'react';


const LoadingScreenContainer = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    align-items: center;
    justify-content: center;
`;

export const LoadingScreen: React.FC = () => {
    return (
        <LoadingScreenContainer>
            Loading
        </LoadingScreenContainer>
    )
}