import { styled, themeValue } from '@jarvis-network/ui';
import React from 'react';
import { JRT_ADDRESS } from '../../apis/data/contract-addresses';
import { useReduxSelector } from '../../state/useReduxSelector';
import { formatNumberValue } from '../../utils';
import { StatBox } from '../StatBox/StatBox';
import { TokenAreaChart } from '../TokenAreaChart/TokenAreaChart';




const Container = styled.div`
    display: grid;
    padding: 10px;
    grid-template-columns: 1fr 1fr;
    color: ${themeValue({}, theme => theme.common.primary)};
`;

const ChartColumn = styled.div`
    overflow-wrap: break-word;
    position: relative;
`;

const StatsColumn = styled.div`
    display: grid;
    padding: 12.5%;
    grid-template-columns: 50% 50%;
    grid-template-rows: repeat(3, 1fr);
    grid-gap: 10%;
`;

const TokenHead = styled.div`
    display: grid;
    justify-items: center;

    p {
        margin: 0.25em 0;
    }
`;

const TokenIcon = styled.img`
    width: 75px;
    height: 75px;
`;
const TokenName = styled.h4`
    margin: 0.25em 0;
    color: ${themeValue({}, theme => theme.text.primary)}
`;

export const NetworkTabContent: React.FC = (props) => {
    const jrtMcap = useReduxSelector(state => state.jrtData.currentMarketcapUSD);
    const jrtPrice = useReduxSelector(state => state.jrtData.currentPriceUSD);
    const jrtHolderCount = useReduxSelector(state => state.jrtData.holderCount);
    const TVL = useReduxSelector(state => state.windowsTVLData.currentTVL);

    const derivativeList = useReduxSelector(state => state.derivativeList);

    const synthTotalMarketcap = derivativeList.reduce((mcap, derivative) => mcap + derivative.totalValue, 0)

    return (
        <Container>
            <ChartColumn>                
                <TokenHead>
                    <TokenIcon src="icons/jrt.png"/>
                    <TokenName>Jarvis Reward Token</TokenName>
                    <p>{formatNumberValue(jrtPrice, 'priceUSD')}</p>
                </TokenHead>
                <TokenAreaChart 
                    tokenAddress={JRT_ADDRESS} 
                    forceDataKind="dex-price"
                    height={'70vh'}
                />
            </ChartColumn>

            <StatsColumn>
                <StatBox 
                    type="marketcap"
                    label={"JRT Marketcap"}
                    value={jrtMcap}
                    sourceExplanation={"Obtained from Coingecko"}
                />
                <StatBox 
                    label={"JRT holders"}
                    value={jrtHolderCount}
                    sourceExplanation={"Obtained from the Synthereum holder tracking subgraph"}
                />
                <StatBox 
                    label={"Number of tradables jSynths"}
                    value={derivativeList.length}
                    sourceExplanation={"Obtained from the Synthereum subgraph"}
                />
                <StatBox 
                    type="marketcap"
                    label={"jSynths Marketcap"}
                    value={synthTotalMarketcap}
                    sourceExplanation={"Obtained from the Synthereum subgraph"}
                />
                <StatBox 
                    type="marketcap"
                    label={"TVL"}
                    value={TVL}
                    sourceExplanation={"Obtained from the Synthereum subgraph"}
                />
            </StatsColumn>

        </Container>
    )           
}