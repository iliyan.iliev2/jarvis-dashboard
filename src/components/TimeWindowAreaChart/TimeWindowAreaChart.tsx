import { styled, themeValue } from '@jarvis-network/ui';
import React, { PureComponent } from 'react';
import { Area, AreaChart as RChartsAreaChart, ResponsiveContainer, Tooltip, TooltipProps, XAxis, YAxis } from 'recharts';
import { Payload } from 'recharts/types/component/DefaultTooltipContent';
import { formatDate, formatNumberValue, TimestampedValue, TimeWindow, unixTimestampToDate } from '../../utils';
import { TimeWindowSelector } from './TimeWindowSelector';

const LINE_COLOR = {
	yellow: '#D9AB44',
	red: '#F02D2D',
	green: '#53B167',
	purple: '#42217E',
};

export type TimeWindowAreaChartProps = {
	timeWindow: TimeWindow,
	data: TimestampedValue[],
	additionalData?: TimestampedValue[],
	additionalDataName?: string,
    onTimeWindowChange: (timeWindow: TimeWindow) => any,
}

const Container = styled.div`
	position: relative;
	height: 100%;
	width: 100%;
`;


const ChartToolTip = styled.div`
	color: ${themeValue({}, theme => theme.text.primary)}
`;

export default class TimeWindowAreaChart extends PureComponent<TimeWindowAreaChartProps> {

	render() {
		
		let time_window = this.props.timeWindow;
		const interval = (time_window <= TimeWindow.WEEK) ? 100 : 2000;

		function tooltipDateFormatter(date: Date){
			return formatDate(date, time_window, {hour: "2-digit"});
		}
	
		function XAxisTickFormatter(date: Date){
			return formatDate(date, time_window, {weekday: false, month: '2-digit', hour: undefined});
		}

		function YAxisTickFormatter(value: number){
			return String(formatNumberValue(value, dataNumberType, {compactIfHigh: true}));
		}

		const dataNumberType = this.props.data[0]?.numberType;
		const additionnalDataNumberType = this.props.additionalData?.[0]?.numberType;

		const CustomTooltip = ({ active, payload, label }: TooltipProps<any, any>) => {
	
			if (active && payload && payload.length !== 0) {
			  return (
				<ChartToolTip>
				  <p className="label">{tooltipDateFormatter(label)}</p>
				  <p>
					  {  
					  	formatNumberValue(Number(payload[0].value), dataNumberType)
					  }
					</p>

				  {
				  	payload[1] && 
					  	<p>
							{this.props.additionalDataName || ''}
							{formatNumberValue(Number(payload[1].value), additionnalDataNumberType)}
						</p>
				  }
				</ChartToolTip>
			  );
			}
		  
			return null;
		};


		const data = [
			... this.props.data.map(data_point => ({
				date: unixTimestampToDate(data_point.unixTimestamp),
				value1: data_point.value,
				value2: this.props.additionalData?.find(e => e.unixTimestamp === data_point.unixTimestamp)?.value
			})),
		];

		return (
			<Container>
				<TimeWindowSelector onTimeWindowChange={this.props.onTimeWindowChange}/>

				{
					(this.props.data.length === 0) ? <div>Loading</div>
					: 

					<ResponsiveContainer>
						<RChartsAreaChart
							width={800}
							height={250}
							data={data}
							margin={{ top: 10, right: 40, left: 0, bottom: 0 }}
						>
						<defs>
							<linearGradient id="color1" x1="0" y1="0" x2="0" y2="1">
								<stop offset="5%" stopColor={LINE_COLOR['purple']} stopOpacity={0.6} />
								<stop offset="95%" stopColor={LINE_COLOR['purple']} stopOpacity={0} />
							</linearGradient>
							<linearGradient id="color2" x1="0" y1="0" x2="0" y2="1">
								<stop offset="5%" stopColor={LINE_COLOR['green']} stopOpacity={0.8} />
								<stop offset="30%" stopColor={LINE_COLOR['green']} stopOpacity={0} />
							</linearGradient>
						</defs>


							<XAxis interval={interval} dataKey="date" tickFormatter={XAxisTickFormatter} />
							<YAxis tickFormatter={YAxisTickFormatter} />

							<Tooltip
									content={CustomTooltip}
									formatter={(amount: number, name: string, payload: Payload<any, any>) => {
										let newName = name;
										let newAmount: number | string = amount;
										newName = 'USD';
										newAmount = `$${new Intl.NumberFormat().format(amount)}`;
										return [newAmount, newName];
									}}
								/>
						
							<Area 
								type="monotone" 
								dataKey="value2" 
								stroke={LINE_COLOR['green']}
								fillOpacity={1}
								fill="url(#color2)"
							/>
							<Area 
								type="monotone" 
								dataKey="value1" 
								stroke={LINE_COLOR['purple']}
								fillOpacity={1}
								fill="url(#color1)"
							/>

						</RChartsAreaChart>
					</ResponsiveContainer>
				}
			</Container>
		);
	}
}
