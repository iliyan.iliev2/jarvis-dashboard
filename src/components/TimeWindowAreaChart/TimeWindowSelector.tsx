import { styled, themeValue } from '@jarvis-network/ui';
import React from 'react';
import { TimeWindow, TIME_WINDOWS } from '../../utils';

type Props = {
    onTimeWindowChange: (timeWindow: TimeWindow) => any,
}

const Container = styled.div`
    display: flex;
    justify-content: flex-end;
    z-index: 1;
    position: absolute;
    top: 2%;
    right: 1vw;
    padding-right: 50px;
    gap: 5px;
    width: 100%;

    color: ${themeValue({}, theme => theme.common.primary)};
    
    > div:last-child {
        margin-right: 15px;
    }

    > div:hover {
        cursor: pointer;
        filter: brightness(1.25);
    }

    > div:hover::after {
        background: rgba(50, 50, 50, 0.500);
        border-radius: 10%;
        width: max-content;
        position: absolute;
        content: attr(data-hover-text);
        text-align: left;
    }

   
`;

export const TimeWindowSelector: React.FC<Props> = ({onTimeWindowChange}) => {

    return (
        <Container>
            {
                TIME_WINDOWS.map(timeWindow => {

                    const hoverText = timeWindow.replaceAll('_', ' ').toLowerCase().slice(1);

                    return <div key={timeWindow} onClick={() => onTimeWindowChange(timeWindow)} data-hover-text={hoverText}> 
                        {timeWindow.slice(0, 1)} 
                    </div>
                })
            }
        </Container>
    )
}