import { styled, themeValue } from '@jarvis-network/ui';
import React from 'react';

const Container = styled.div`
    padding: 10px;
    height: 100%;
    overflow: hidden;
    color: ${themeValue({}, theme => theme.text.primary)};
`;

export const FarmingTabContent: React.FC = (props) => {

    return (
        <Container>
        </Container>
    )           
}