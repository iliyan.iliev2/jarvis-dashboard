import { Radio, RadioGroup, styled, themeValue } from '@jarvis-network/ui';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setFocusedDerivativeDataKind } from '../../state/slices/app';
import { derivativeSliceSelector, getAllDerivativeSupplyAndTradeDataForTimeWindow, getDerivativePricesForTimeWindow, getSynthTokenDexInfoForTimeWindow } from '../../state/slices/derivatives';
import { getJRTPriceUSDForTimeWindow } from '../../state/slices/jrt';
import { useReduxSelector } from '../../state/useReduxSelector';
import { capitalize, removeDashs, TimeWindow } from '../../utils';
import TimeWindowAreaChart from '../TimeWindowAreaChart/TimeWindowAreaChart';

const Container = styled.div<{width?: string, height?: string}>`
    height: ${props => props.height || 'auto'};
    width: ${props => props.width || 'auto'};
    color: ${props => props.theme.text.primary};

    position: relative;

    .choices {
        position: absolute;
        display: none;
        top: 0;
        left: 50%;
        width: max-content;
        background: ${themeValue({}, theme => theme.background.secondary)};
        border-radius: 5px;
        justify-items: start;
        cursor: pointer;
    }

    .kind-selection:hover {
        cursor: pointer;
    }

    .kind-selection:hover h3 {
        filter: brightness(0.5);
    }

    .kind-selection:hover .choices, .choices:hover {
        z-index: 2;
        display: grid;
        cursor: pointer;
    }

`;

export type TokenDataKind = 
    'supply' | 'price' | 'dex-price' | 'all-prices' | 'total-value' | 'total-traded-token-volume' | 'total-traded-usd-volume'
    | 'traded-token-volume' | 'traded-usd-volume'
    | 'dex-total-traded-usd-volume'
    | 'dex-traded-usd-volume'
    | 'dex-total-trade-count'
    | 'synthereum-total-trade-count'

export type TokenAreaChartProps = {
    tokenAddress: string,
    derivativeAddress?: string,
    forceDataKind?: TokenDataKind,
    width?: string,
    height?: string,
}

export const TokenAreaChart: React.FC<TokenAreaChartProps> = ({
    tokenAddress, 
    derivativeAddress, 
    forceDataKind: forceShownDataKind, 
    width,
    height,
}) => {

    const derivativeList = useReduxSelector(state => state.derivativeList);
    const tokenSymbol = derivativeList.find(d => d.tokenCurrencyAddress === tokenAddress)?.symbol;
    const tokenData = useReduxSelector(derivativeSliceSelector(tokenSymbol || 'jrt'));
    
    const [timeWindow, setWindow] = useState(TimeWindow.WEEK);
    const [shownDataKind, setShownDataKind] = useState<TokenDataKind>('price');

    const windowBlocks = useReduxSelector(state => state.windowsBlocks);

    const dispatch = useDispatch();

    useEffect(() => {
        if(tokenSymbol){
            dispatch(getAllDerivativeSupplyAndTradeDataForTimeWindow({timeWindow}))
            dispatch(getDerivativePricesForTimeWindow({
                timeWindow, 
                tokenCurrencyAddress: tokenAddress,
                symbol: tokenSymbol
            }));
            dispatch(getSynthTokenDexInfoForTimeWindow({timeWindow, tokenCurrencyAddress: tokenAddress, tokenSymbol}))
        } else {
            dispatch(getJRTPriceUSDForTimeWindow(timeWindow));
        }
    }, [timeWindow, derivativeList, dispatch]);

    const _shownDataKind = forceShownDataKind || shownDataKind;
    
    const handleDataKindChoice = (kind: string) => {
        setShownDataKind(kind as TokenDataKind)
        dispatch(setFocusedDerivativeDataKind(kind as TokenDataKind));
    }

    return (
        <Container width={width} height={height}>
            {
                forceShownDataKind ? 
                <h3></h3>
                :
                <div className='kind-selection'>
                    <h3>{removeDashs(capitalize(_shownDataKind))}</h3>
                    <RadioGroup value={shownDataKind} onChange={handleDataKindChoice} className='choices'>
                        <Radio value="price" name="price">Price</Radio>
                        <Radio value="dex-price" name="dex-price">Dex Price</Radio>
                        <Radio value="all-prices" name="all-prices">All prices</Radio>
                        <Radio value="supply" name="supply">Supply</Radio>
                        <Radio value="total-value" name="total-value">Total value</Radio>
                        <Radio value="total-traded-token-volume" name="total-traded-token-volume">Total traded token volume</Radio>
                        <Radio value="total-traded-usd-volume" name="total-traded-usd-volume">Total traded USD volume</Radio>
                        <Radio value="traded-token-volume" name="traded-token-volume">Traded volume</Radio>
                        <Radio value="traded-usd-volume" name="traded-usd-volume">Traded volume (USD)</Radio>
                        <Radio value="dex-total-traded-usd-volume" name="dex-total-traded-usd-volume">Dexs total traded USD volume</Radio>
                        <Radio value="dex-traded-usd-volume" name="dex-traded-usd-volume">Dexs traded USD volume</Radio>
                        <Radio value="dex-total-trade-count" name="dex-total-trade-count">Dexs total trade count</Radio>
                        <Radio value="synthereum-total-trade-count" name="synthereum-total-trade-count">Synthereum total trade count</Radio>
                    </RadioGroup>
                </div>
            }
            <TimeWindowAreaChart 
                timeWindow={timeWindow}
                data={
                    (
                        (_shownDataKind === 'price' || _shownDataKind === 'all-prices') ? tokenData?.windowsUSDPrices[timeWindow]
                        : _shownDataKind === 'dex-price'  ? tokenData?.windowsDexUSDPrices[timeWindow]
                        : _shownDataKind === 'supply'  ? tokenData?.windowsSupplyValues[timeWindow]
                        : _shownDataKind === 'total-value'  ? tokenData?.windowsTotalValues[timeWindow]
                        : _shownDataKind === 'total-traded-token-volume'  ? tokenData?.windowsTotalTradedTokenVolume[timeWindow]
                        : _shownDataKind === 'total-traded-usd-volume'  ? tokenData?.windowsTotalTradedVolumeUSDValues[timeWindow]
                        : _shownDataKind === 'traded-token-volume'  ? tokenData?.windowsTradedTokenVolume[timeWindow]
                        : _shownDataKind === 'dex-total-traded-usd-volume'  ? tokenData?.windowsDexTotalTradedVolumeUSDValues[timeWindow]
                        : _shownDataKind === 'dex-traded-usd-volume'  ? tokenData?.windowsDexTradedUSDVolumes[timeWindow]
                        : _shownDataKind === 'dex-total-trade-count'  ? tokenData?.windowsDexTotalTradeCount[timeWindow]
                        : _shownDataKind === 'synthereum-total-trade-count'  ? tokenData?.windowsTotalTradeCount[timeWindow]
                        : tokenData?.windowsTradedUSDVolumes[timeWindow] || []
                    ) || []
                }
                additionalData={
                    _shownDataKind === 'all-prices' ? tokenData?.windowsDexUSDPrices[timeWindow] : undefined
                }
                additionalDataName={
                    _shownDataKind === 'all-prices' ? 'Dex price: ' : undefined
                }
                onTimeWindowChange={setWindow}
            />
        </Container>
    )
}
