import { styled, themeValue, Tooltip } from '@jarvis-network/ui';
import React from 'react';
import { formatNumberValue, NumberType } from '../../utils';

const Container = styled.div`
    border: ${themeValue({}, theme => theme.border.primary)};
    border-radius: ${themeValue({}, theme => theme.borderRadius.s)};
    border-style: solid;
    background:  ${themeValue({}, theme => theme.background.medium)};
    position: relative;
   color: ${themeValue({}, theme => theme.common.primary)};

    height: max-content;

    :hover
`;

const TooltipWrapper = styled.div`
    height: 100%;
    width: 100%;
    position: absolute;
    cursor: pointer;

    :hover > * > * {
        width: 200px;
        overflow-wrap: break-word;
        visibility: visible !important;
    }
`


export type StatBoxProps = {
    label: string, 
    value: number, 
    description?: string, 
    sourceExplanation?: string,
    type?: NumberType,
};



const Label = styled.h3`
    font-size: ${props => props.theme.font.sizes.l};
    color: ${themeValue({}, theme => theme.text.primary)};
`;

const ValueText = styled.h4`
    font-size: ${props => props.theme.font.sizes.m};
    color: ${themeValue({}, theme => theme.common.success)};
`;

const DescriptionText = styled.p`
    font-size: ${props => props.theme.font.sizes.m};
`;

export const StatBox: React.FC<StatBoxProps> = ({type, label, value, description, sourceExplanation, children}) => {
    return (
        <Container>
            {
                sourceExplanation && 
                <TooltipWrapper>
                    <Tooltip tooltip={
                        <div>
                            {sourceExplanation}
                            {children}
                        </div>
                    } 
                        position="right" width="max-content"/>
                </TooltipWrapper>
            }

            <Label>{label}</Label>
            <ValueText>{formatNumberValue(value, type)}</ValueText>
            <DescriptionText>{description}</DescriptionText>
        </Container>
    )           
}