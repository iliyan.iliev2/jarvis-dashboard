import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { loadFirstTabContent } from '../state/slices/app';
import { getBlocksForAllTimeWindows } from '../state/slices/blocks';
import { useReduxSelector } from '../state/useReduxSelector';
import { Background } from './Background';
import { FarmingTabContent } from './FarmingTabContent/FarmingTabContent';
import { LoadingScreen } from './LoadingScreen';
import { Navigation } from './Navigation/Navigation';
import { NetworkTabContent } from './NetworkTabContent/NetworkTabContent';
import { SynthsTabContent } from './SynthsTabContent/SynthsTabContent';



export const ContentRoot: React.FC = (props) => {
    const theme = useReduxSelector(state => state.theme);
    const firstTabDataLoading = useReduxSelector(state => state.app.firstTabDataLoading);
    const selectedTab = useReduxSelector(state => state.app.tab);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadFirstTabContent());
    }, [dispatch]);

    useEffect(() => {
        dispatch(getBlocksForAllTimeWindows())
    }, [dispatch]);


    if(firstTabDataLoading) return <LoadingScreen/>

    return (
        <Background image={theme.backgroundUrl} height={selectedTab === 1 ? '200vh' : '100vh'}>
            <Navigation/>
            {
                  (selectedTab === 0) ?
                    <NetworkTabContent/>
                : (selectedTab === 1) ? 
                    <SynthsTabContent/>
                : <FarmingTabContent/>
            }
        </Background>
    )
}
