import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getSynthereumTVLForTimeWindow } from '../../state/slices/tvl';
import { useReduxSelector } from '../../state/useReduxSelector';
import { TimeWindow } from '../../utils';
import TimeWindowAreaChart from '../TimeWindowAreaChart/TimeWindowAreaChart';




export const TVLAreaChart: React.FC<{className: string}> = (props) => {
    
    const historicalWindowTvlData = useReduxSelector(state => state.windowsTVLData.historical);
    const [timeWindow, setTimeWindow] = useState(TimeWindow.WEEK);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getSynthereumTVLForTimeWindow(timeWindow));
    }, [dispatch, timeWindow])

    return (
        <div {...props}>

            <TimeWindowAreaChart
                timeWindow={timeWindow}
                data={historicalWindowTvlData[timeWindow]}
                onTimeWindowChange={(timeWindow) => setTimeWindow(timeWindow)}
                {...props}
            />
        </div>
    )
}
