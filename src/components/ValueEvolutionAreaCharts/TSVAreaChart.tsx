import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getSynthereumTSVForTimeWindow } from '../../state/slices/tsv';
import { useReduxSelector } from '../../state/useReduxSelector';
import { TimeWindow } from '../../utils';
import TimeWindowAreaChart from '../TimeWindowAreaChart/TimeWindowAreaChart';



export const TSVAreaChart: React.FC<{className: string}> = (props) => {
    const [timeWindow, setTimeWindow] = useState(TimeWindow.WEEK);
    const windowTSVData = useReduxSelector(state => state.windowsTSVData);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getSynthereumTSVForTimeWindow(timeWindow));
    }, [timeWindow, dispatch])

    return (
        <div {...props}>
            <TimeWindowAreaChart
                timeWindow={timeWindow}
                data={windowTSVData[timeWindow]}
                onTimeWindowChange={(timeWindow) => setTimeWindow(timeWindow)}
            />
        </div>
    )
}
