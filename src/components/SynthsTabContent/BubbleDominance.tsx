import { styled, themeValue } from "@jarvis-network/ui";
import React, { MouseEventHandler } from "react";

export type BubbleDominanceData = {
    name: string, 
    dominance: string | number, 
    imageURL?: string
};

export type BubbleDominanceProps = {
    rawData: BubbleDominanceData[],
    onClickedBubble?: (data: any) => any
}


const Container = styled.div`
    display: block;
    width: 100%
    height: 100%;
    overflow: hidden;
`;

const Bubble = styled.div`
    position: relative;

    :hover {
        cursor: pointer;
    }

    :not(:hover) h4 {
        display: none;
    }

    :hover h4 {
        display: block;
        z-index: 1;
        position: absolute;
        margin: 0;
        top: 33%;
        left: 50%;
        transform: translateX(-50%);
        background: none;
        color: ${themeValue({}, theme => theme.text.primary)};
    }

    img {
        width: 100%
        height: 100%;
        overflow: hidden;
    }

    :hover img {
        filter: brightness(0.8);
    }
`;


const BubbleComponent = ({data, onClick}: {data: BubbleDominanceData, onClick?: MouseEventHandler<unknown>}) => {
    let dominance = Number(data.dominance);
    return (
        <Bubble onClick={onClick}>
            <img 
                alt={data.name} key={data.name} src={data.imageURL || 'icons/jrt.png'} width={ (1.5 * dominance) + 'px'}
            />
            <h4>{data.name} - {dominance.toFixed(2)}% </h4>
        </Bubble>
       
    );
}

export const BubbleDominance = ({rawData, onClickedBubble: onClickedBubble}: BubbleDominanceProps) => {

    return (
        <Container>
            {
                rawData.map(dominanceData => (
                    <BubbleComponent 
                        key={dominanceData.name} data={dominanceData} 
                        onClick={onClickedBubble && (() => onClickedBubble(dominanceData))}
                    />
                ))
            }
        </Container>
    )

}