import { ColumnType, DataGrid, DataGridColumnProps, styled } from "@jarvis-network/ui";
import React from "react";
import { useDispatch } from "react-redux";
import { CellInfo } from "react-table";
import { setFocusedDerivative } from "../../state/slices/app";
import { useReduxSelector } from "../../state/useReduxSelector";
import { formatNumberValue } from "../../utils";
import { DominanceData, SynthsPieChart } from "./SynthsPieChart";


const SyntheticNameCell = (cellData: CellInfo) => {
    return (
        <span style={{cursor: 'pointer'}} onClick={() => cellData.tdProps.rest.dispatch(setFocusedDerivative(cellData.original))}> 
            {cellData.value} 
        </span>
    )
};

const SupplyCell = (cellData: CellInfo) => {
    return <span> {formatNumberValue(cellData.value, 'simple-format')} </span>
};

const PriceUSDCell = (cellData: CellInfo) => {
    return <span> {formatNumberValue(cellData.value, 'priceUSD')} </span>
};

const ValueUSDCell = (cellData: CellInfo) => {
    return <span> {formatNumberValue(cellData.value, 'valueUSD')} </span>
};

const PercentageCell = (cellData: CellInfo) => {
    return <span> {formatNumberValue(cellData.value, 'percentage')} </span>
};

const MIN_WIDTH = 80;
const columns: DataGridColumnProps[] = [
    {
        header: 'Synthetic',
        key: 'symbol',
        type: ColumnType.CustomCell,
        cell: SyntheticNameCell,
        minWidth: MIN_WIDTH
    },
    {
        header: 'Price',
        key: 'priceUSD',
        type: ColumnType.CustomCell,
        minWidth: MIN_WIDTH,
        cell: PriceUSDCell
    },
    {
        header: 'Collateral',
        key: 'collateralBalance',
        type: ColumnType.CustomCell,
        minWidth: MIN_WIDTH,
        cell: ValueUSDCell
    },
    {
        header: 'Supply',
        key: 'supply',
        type: ColumnType.CustomCell,
        minWidth: MIN_WIDTH,
        cell: SupplyCell,
    },
    {
        header: 'Total value',
        key: 'totalValue',
        type: ColumnType.CustomCell,
        minWidth: MIN_WIDTH,
        cell: ValueUSDCell
    },
    {
        header: 'Dominance',
        key: 'dominance',
        type: ColumnType.CustomCell,
        minWidth: MIN_WIDTH,
        cell: PercentageCell
    },
    {
        header: 'Total volume (all time)',
        key: 'totalTradedVolumeUSD',
        type: ColumnType.CustomCell,
        minWidth: MIN_WIDTH,
        cell: ValueUSDCell
    },
];

const Container = styled.div`
    display: block;
    width: 100%
    height: 100%;
    overflow: hidden;
    padding: 5px;
`;

const Dominance = styled.div`
    display: grid;
    width: 100%
    height: 100%;
    overflow: hidden;
    grid-template-columns: 1fr 3fr;

    [role=grid] {
        display: grid;
        grid-template-rows: 1fr 3fr;
    }
    
    [role=row] {
        display: grid;
        grid-template-columns: repeat(${columns.length}, 1fr);
    }
`;

export const SynthsDominance: React.FC<{}> = () => {

    const derivativeList = useReduxSelector(state => state.derivativeList);


    const totalValueSum = derivativeList.reduce((prev, e) => prev + e.priceUSD * e.supply, 0);
    const data = [...derivativeList]
        .sort((d1, d2) => d2.totalValue - d1.totalValue)
        .map(d => {
        
        return  {
            ...d,
            dominance: 100 * d.totalValue / totalValueSum,
        };
    })

    const bubbleDataList: DominanceData[] = data.map(d => {
        return  {
            name: d.symbol,
            tokenCurrencyAddress: d.tokenCurrencyAddress,
            imageURL: d.imageURL,
            dominance: 100 * d.totalValue / totalValueSum,
        };
    })
    
    const dispatch = useDispatch();

    return (
        <Container>
            <Dominance>
                <SynthsPieChart rawData={bubbleDataList} />
                {/* <BubbleDominance rawData={bubbleDataList} onClickedBubble={synth => dispatch(setFocusedDerivative(synth))}/> */}
                <DataGrid 
                    columns={columns} 
                    data={data}
                    resizable={true} 
                    sortable={false}
                    loading={false}
                    showPagination={false}
                    loadingText=''
                    minRows={1}
                    getTdProps={() => ({dispatch})}
                />

            </Dominance>
        </Container>
    )
}