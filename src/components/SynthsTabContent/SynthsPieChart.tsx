import { styled, themeValue } from "@jarvis-network/ui";
import React from "react"
import { Cell, Pie, PieChart, PieProps, ResponsiveContainer } from "recharts"
import { formatNumberValue } from "../../utils";

//inspired from https://bit.dev/recharts/recharts/pie-chart?example=5cecfbc716ee3000144d62da

export type DominanceData = {
    name: string, 
    dominance: string | number, 
};

export type SynthsPieChartProps = {
    rawData: DominanceData[],
}


const RADIAN = Math.PI / 180;
const PIE_SLICE_COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const StyledText = styled.text`
    fill: ${themeValue({}, theme => theme.text.primary)};
`;


const Label: PieProps['label'] = ({
	cx, cy, midAngle, innerRadius, outerRadius, percent, name
}) => {
	const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
	const textX = cx + radius * Math.cos(- midAngle * RADIAN);
	const textY = cy + radius * Math.sin(- midAngle * RADIAN);
    const textAnchor = textX > cx ? 'start' : 'end';

	return (
		<StyledText x={textX} y={textY} textAnchor={textAnchor} dominantBaseline="central">
			{name} {formatNumberValue(percent * 100, 'percentage')}
		</StyledText> as any as SVGElement
	);
};


export const SynthsPieChart: React.FC<SynthsPieChartProps> = ({rawData}) => {
    return (
        <ResponsiveContainer>
            <PieChart width={700} height={300}>
                <Pie 
                    data={rawData} dataKey="dominance" nameKey="name" 
                    cx="50%" cy="50%" outerRadius={50} fill="#8884d8"
                    labelLine={false} label={Label}
                >

                    {
                        rawData.map(
                            (entry, index) => (
                                <Cell 
                                    key={`cell-${index}`} 
                                    fill={PIE_SLICE_COLORS[index % PIE_SLICE_COLORS.length]} 
                                />
                            )
                        )
                    }
                    </Pie>
            </PieChart>
        </ResponsiveContainer>
     
    )
}