import { styled, themeValue } from '@jarvis-network/ui';
import React from 'react';
import { DEFI_INTEGRATION } from '../../data/defi-integration';

const Container = styled.div`
    display: block;
`;

const Section = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: start;
    font-size: ${themeValue({}, theme => theme.font.sizes.m)};

    h4 {
        text-decoration: underline;
        margin: 0.75em 0;
    }

    span {
        color: ${themeValue({}, theme => theme.text.medium)};
        font-size: ${themeValue({}, theme => theme.font.sizes.m)};
    }
`;


export const DeFiIntegration: React.FC<{className?: string}> = (props) => {

    return (
        <Container {...props}>
            <Section>
                <h4> DEX AMMs </h4> 
                <span>
                    { DEFI_INTEGRATION.DEX_AMMs}
                </span>
            </Section>
            
            <Section>
                <h4> Money markets </h4>
                <span>
                    { DEFI_INTEGRATION.MoneyMarkets }
                </span>
            </Section>

            <Section>
                <h4>
                    Vault automation
                </h4>
                <span>
                    { DEFI_INTEGRATION.VaultAutomation }
                </span>
            </Section>

            <Section>
                <h4>
                    CeFi / CeDeFi / TradFi :
                </h4>
                <span>
                    { DEFI_INTEGRATION.XFi }
                </span>
            </Section>
        </Container>
    )
}
