import { styled, themeValue } from '@jarvis-network/ui';
import React from 'react';
import { useReduxSelector } from '../../state/useReduxSelector';
import { SyntheticInfo } from '../SyntheticInfo/SyntheticInfo';
import { TSVAreaChart } from '../ValueEvolutionAreaCharts/TSVAreaChart';
import { TVLAreaChart } from '../ValueEvolutionAreaCharts/TVLAreaChart';
import { SynthsDominance } from './SynthsDominance';

const Container = styled.div`
   height: 180vh;
   width: 100%;
`;

const GeneralSyntheticsInfoContainer = styled.div`
    display: grid;
    height: 90vh;
    width: 100%;
    padding: 10px;
    grid-template-columns: 50% 50%;
    grid-template-rows: 40% 60%;
    grid-gap: 5%;
    overflow: hidden;
    color: ${themeValue({}, theme => theme.text.primary)};
`;


export const Section = styled.div<{gridColumn?: string}>`
    height: 100%;
    width: 100%;
    grid-column: ${props => props.gridColumn ?? 'auto'};

    h3 {
        font-size: ${themeValue({}, theme => theme.font.sizes.l)};
    }

    > .chart {
        height: 85%;
        width: 90%;
    }
`;

export const SynthsTabContent: React.FC = (props) => {
    const derivativeList = useReduxSelector(state => state.derivativeList);
    const blocksLoading = useReduxSelector(state => state.app.blocksLoading);

    if(blocksLoading || derivativeList.length === 0) {
        return (
            <div> 
                Loading, 
                blocks loading : {blocksLoading.toString()},
                derivatives = { JSON.stringify(derivativeList)}
            </div>
        );
    }


    return (
        <Container>
            <GeneralSyntheticsInfoContainer>

                <Section>
                    <h3> Total Synthetic Value Evolution </h3>
                    <TSVAreaChart className="chart" />
                </Section>

                <Section>
                    <h3> TVL Evolution </h3>
                    <TVLAreaChart className="chart" />
                </Section>


                <Section gridColumn='1/3'>
                    <h3> jSynths Dominance </h3>
                    <SynthsDominance/>
                </Section>

                {/* <Section >
                    <h3> DeFi Integration </h3>
                    <DeFiIntegration/>
                </Section> */}
            </GeneralSyntheticsInfoContainer>

            <SyntheticInfo/>
        </Container>
    )           
}