import { Tabs } from "@jarvis-network/ui";
import { TabsProps } from "@jarvis-network/ui/dist/Tabs";
import { Tab } from "@jarvis-network/ui/dist/Tabs/types";
import React from "react";


export const NavigationTabs = (props: Omit<TabsProps, 'tabs'>) => {
    const tabs: Tab[] = ['Network', 'Synths', 'Farming'].map(title => ({title}));
    return (
      <Tabs
        {...props}
        tabs={tabs}
      />
    );
  };