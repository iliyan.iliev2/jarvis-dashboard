import React from "react";

import { styled, themeValue } from "@jarvis-network/ui";
import { NavigationTabs } from "./NavigationTabs";
import { useDispatch } from "react-redux";
import { useReduxSelector } from "../../state/useReduxSelector";
import { setTab } from "../../state/slices/app";

const NavigationDiv = styled.div`
    margin-right: 10px;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    border-bottom: 1px solid black;
    border-bottom-color: ${themeValue({}, theme => theme.border.primary)};
    width: 100%;

    background: ${themeValue({}, theme => theme.background.secondary)};

    > div {
        padding-right: 15px;
    }

    > div > div {
        border-bottom: none;
    }
`;

export const Navigation = () => {
    const selectedTabIndex = useReduxSelector(state => state.app.tab);
    const dispatch = useDispatch();
    const setSelected = (index: number) => dispatch(setTab(index));

    return <NavigationDiv>
        <NavigationTabs onChange={setSelected} selected={selectedTabIndex}></NavigationTabs>
    </NavigationDiv>
}