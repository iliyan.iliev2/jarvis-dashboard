import React from "react"
import { Derivative, derivativeSliceSelector } from "../../state/slices/derivatives";
import { useReduxSelector } from "../../state/useReduxSelector";
import { formatNumberValue } from "../../utils";
import { StatBox } from "../StatBox/StatBox"
import { DexIcon } from "./DexIcon";
import { StatDetails } from "./StatDetails";

export const TotalFeesStatBox = ({focusedDerivative}: {focusedDerivative: Derivative}) => {

    let selectDerivative = derivativeSliceSelector(focusedDerivative);
    useReduxSelector(state => Object.values(selectDerivative(state)?.rawDexsData ?? {}));
    const rawDexData = useReduxSelector(state => selectDerivative(state)?.rawDexsData) ?? {};
    

    const dexTotalFeesGeneratedUSD = useReduxSelector(state => selectDerivative(state)?.dexTotalFeesGeneratedUSD) ?? 0;
    
    const synthereumTotalFeePaid = focusedDerivative.totalFeePaidUSD;

    return (
        <StatBox
            type="valueUSD"
            label="Fees generated"
            value={
                (synthereumTotalFeePaid + dexTotalFeesGeneratedUSD)
            }
            sourceExplanation={
                "Computed using data from the Synthereum subgraph and dex subgraphs"
            }
        >
            <StatDetails 
                synthereumValue={formatNumberValue(synthereumTotalFeePaid, 'valueUSD')}
                dexsTotalValue={formatNumberValue(dexTotalFeesGeneratedUSD, 'valueUSD')}
                dexValues={
                    {
                        Balancer: formatNumberValue(rawDexData.Balancer?.totalFeesGeneratedUSD ?? 0, 'valueUSD'),
                        Sushiswap: formatNumberValue(rawDexData.Sushiswap?.totalFeesGeneratedUSD ?? 0, 'valueUSD')
                    }
                }
            />
        </StatBox>
    );

}