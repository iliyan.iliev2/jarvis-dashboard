import React from "react";
import { Derivative, derivativeSliceSelector } from "../../state/slices/derivatives";
import { useReduxSelector } from "../../state/useReduxSelector";
import { StatBox } from "../StatBox/StatBox";

export const HolderCountStatBox = ({focusedDerivative}: {focusedDerivative: Derivative}) => {

    let selectDerivative = derivativeSliceSelector(focusedDerivative);
    let holderCount = useReduxSelector(state => selectDerivative(state)?.holderCount ?? 0);


    return (
        <StatBox
            type="simple-format"
            label="Holder count"
            value={holderCount}
            sourceExplanation={"Obtained from the Synthereum subgraph and dex subgraphs"}
        >
        </StatBox>
    );

}