import { styled } from '@jarvis-network/ui';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { COLLATERALIZATION_RATIO } from '../../data/derivatives';
import { setFocusedDerivative } from '../../state/slices/app';
import { getDerivativePricesForTimeWindow, getSynthTokenDexInfoForTimeWindow } from '../../state/slices/derivatives';
import { useReduxSelector } from '../../state/useReduxSelector';
import { TimeWindow } from '../../utils';
import { StatBox } from '../StatBox/StatBox';
import { TokenAreaChart } from '../TokenAreaChart/TokenAreaChart';
import { AverageSpreadStatBox } from './AverageSpreatStatBox';
import { ChartExplanation } from './ChartExplanation';
import { HolderCountStatBox } from './HolderCountStatBox';
import { TotalFeesStatBox } from './TotalFeesStatBox';
import { TotalTradeCountStatBox } from './TotalTradeCountStatBox';
import { TotalVolumeStatBox } from './TotalVolumeStatBox';

const Container = styled.div`
    display: grid;
    height: 90vh;
    width: 100%;
    justify-items: center;
`;

const InnerContainer = styled.div`
    display: grid;
    height: 70vh;
    width: 75vw;
    grid-template-columns: 50% 50%;
`;

const ChartColumn = styled.div`
    overflow-wrap: break-word;
    display: grid;
    grid-template-rows: 2fr 1fr;
`;

const StatsColumn = styled.div`
    padding: 15%;
    display: grid;
    grid-template-columns: 50% 50%;
    grid-template-rows: repeat(4, 1fr);
    grid-gap: 5%;
`;


const DerivativeHead = styled.div`
    position: relative;
    width: 100px;

    :hover > img {
        display: none;
    }

    :not(:hover) .select {
        display: none;
    }

    .select img {
        cursor: pointer;
    }

    .select img:hover {
        filter: brightness(0.8);
    }
`;

const DerivativeSelect = styled.div<{choiceCount: number}>`
    display: grid;
    grid-template-columns: repeat(${({choiceCount}) => choiceCount}, 1fr);
    justify-items: center;
`;


const TokenTitle = styled.h3`
    font-size: ${props => props.theme.font.sizes.xxl};
    color: ${props => props.theme.text.primary};
`;

export const SyntheticInfo: React.FC<{}> = () => {
    const derivativeList = useReduxSelector(state => state.derivativeList);    
    const focusedDerivative = useReduxSelector(state => state.app.focusedDerivative);    
    const dispatch = useDispatch();

    useEffect(() => {
        if(focusedDerivative){
            dispatch(getSynthTokenDexInfoForTimeWindow({
                timeWindow: TimeWindow.MONTH, 
                tokenCurrencyAddress: focusedDerivative.tokenCurrencyAddress,
                tokenSymbol: focusedDerivative.symbol
            }));
            dispatch(getDerivativePricesForTimeWindow({
                timeWindow: TimeWindow.MONTH, 
                tokenCurrencyAddress: focusedDerivative.tokenCurrencyAddress,
                symbol: focusedDerivative.symbol
            }));
        }
    }, [focusedDerivative, dispatch]);



    let potentiallyMintableAmountUSD = 
        (focusedDerivative?.remainingPoolsCollateral ?? 0) 
        / (COLLATERALIZATION_RATIO - 1);


    return (
        <Container>
         
            <DerivativeHead>
                <img 
                    alt={focusedDerivative?.symbol} src={focusedDerivative?.imageURL || 'icons/jrt.png'}
                    width="100px"
                />
                <DerivativeSelect choiceCount={derivativeList.length} className="select">
                    {
                        derivativeList.map(d => (
                            <div key={d.symbol} onClick={() => dispatch(setFocusedDerivative(d))}>
                                <img 
                                    alt={d.symbol} src={d.imageURL || 'icons/jrt.png'}
                                    width="100px"
                                />
                            </div>
                        ))
                    }
                </DerivativeSelect>
                <TokenTitle>{focusedDerivative?.symbol}</TokenTitle>
            </DerivativeHead>
            {
                focusedDerivative ? 
                <InnerContainer>
                    <ChartColumn>
                        <TokenAreaChart 
                            tokenAddress={focusedDerivative.tokenCurrencyAddress} 
                            derivativeAddress={focusedDerivative.address} 
                            width='38vw'
                            height='40vh'
                        />
                        <ChartExplanation/>
                    </ChartColumn>
                    <StatsColumn>
                        <TotalVolumeStatBox focusedDerivative={focusedDerivative}/>
                        <TotalFeesStatBox focusedDerivative={focusedDerivative} />
                        <TotalTradeCountStatBox focusedDerivative={focusedDerivative} />
                        <AverageSpreadStatBox focusedDerivative={focusedDerivative} />
                        <HolderCountStatBox focusedDerivative={focusedDerivative} />
                        <StatBox
                            type="valueUSD"
                            label="Mintable value"
                            value={potentiallyMintableAmountUSD}
                            sourceExplanation={"Obtained from the Synthereum subgraph"}
                        />
                    </StatsColumn>
                </InnerContainer>
        
                : <div/>
            }
        </Container>
    )
}
