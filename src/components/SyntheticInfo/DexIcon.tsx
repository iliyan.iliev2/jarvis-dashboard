import React from 'react';
import { styled, themeValue } from "@jarvis-network/ui";
import { Dex } from '../../state/slices/token-slice';

export type DexIconProps = {
    dex: Dex,
    size: string
};

const Container = styled.div<{size: string}>`
    display: inline-block;
    position: relative;

    :hover {
        cursor: pointer;
    }

    :not(:hover) h4 {
        display: none;
    }

    :hover h4 {
        display: block;
        z-index: 1;
        position: absolute;
        margin: 0;
        top: 33%;
        left: 50%;
        transform: translateX(-50%);
        background: none;
        color: ${themeValue({}, theme => theme.text.primary)};
    }

    :hover img {
        filter: brightness(0.8);
    }
`;


export const DexIcon = (props: DexIconProps) => {
    const {dex, size} = props;
    return (
        <Container size={size}>
           <img 
                alt={dex} key={dex} src={`icons/${dex}.png`} width={size} height={size}
            />
            <h4>{dex}</h4>
        </Container>
    )           
}