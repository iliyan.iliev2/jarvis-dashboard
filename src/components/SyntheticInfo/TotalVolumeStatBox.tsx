import React, { useState } from "react"
import { Derivative, derivativeSliceSelector } from "../../state/slices/derivatives";
import { RawDexsData } from "../../state/slices/token-slice";
import { useReduxSelector } from "../../state/useReduxSelector";
import { formatNumberValue } from "../../utils";
import { StatBox } from "../StatBox/StatBox"
import { DexIcon } from "./DexIcon";
import { StatDetails } from "./StatDetails";

export const TotalVolumeStatBox = ({focusedDerivative}: {focusedDerivative: Derivative}) => {

    let selectDerivative = derivativeSliceSelector(focusedDerivative);
    const {
        rawDexsData,
        synthereumTotalTradedVolumeUSD,
        dexTotalTradedVolumeUSD
    } = useReduxSelector(state => ({
        rawDexsData: {} as RawDexsData, 
        synthereumTotalTradedVolumeUSD: 0, 
        dexTotalTradedVolumeUSD: 0,
        ...selectDerivative(state)
    }));
    
    return (
        <StatBox
            type="valueUSD"
            label="Total volume"
            value={synthereumTotalTradedVolumeUSD + dexTotalTradedVolumeUSD}
            sourceExplanation={"Obtained from the Synthereum subgraph and dex subgraphs."}
        >
            <StatDetails 
                synthereumValue={formatNumberValue(synthereumTotalTradedVolumeUSD, 'valueUSD')}
                dexsTotalValue={formatNumberValue(dexTotalTradedVolumeUSD, 'valueUSD')}
                dexValues={
                    {
                        Balancer: formatNumberValue(rawDexsData.Balancer?.totalTradedVolumeUSD ?? 0, 'valueUSD'),
                        Sushiswap: formatNumberValue(rawDexsData.Sushiswap?.totalTradedVolumeUSD ?? 0, 'valueUSD')
                    }
                }
            />
        </StatBox>
    );

}