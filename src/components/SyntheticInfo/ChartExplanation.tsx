import { styled, themeValue } from '@jarvis-network/ui';
import React from 'react';
import { SyntheticDataExplanations } from '../../data/chart-data-explanations';
import { useReduxSelector } from '../../state/useReduxSelector';

export const Container = styled.div`
    font-size: ${themeValue({}, theme => theme.font.sizes.m)};
    color: ${themeValue({}, theme => theme.text.primary)};
`

export const ChartExplanation = () => {
    const dataKind = useReduxSelector(state => state.app.focusedDerivativeData);


    return (
        <Container>
            {
                dataKind ? SyntheticDataExplanations[dataKind] : ''
            }
        </Container>
    )
}

