import { styled } from '@jarvis-network/ui';
import React from 'react';
import { Dex } from '../../state/slices/token-slice';
import { DexIcon } from './DexIcon';

export type StatDetailsProps = {
    synthereumValue: string  | number,
    dexsTotalValue: string | number,
    dexValues: {
        [key in Dex]?: string | number
    }
}
 
const Container = styled.div`
    display: grid;
    grid-template-rows: repeat(4, 1fr);
`;

export const StatDetails: React.FC<StatDetailsProps> = ({synthereumValue, dexsTotalValue, dexValues}) => {
    return (
        <Container>
            <span>Synthereum: {synthereumValue} </span>
            <span>Dexs : {dexsTotalValue}</span>
    
            <span>
                <DexIcon dex="Balancer" size="20px"/> <span>{dexValues.Balancer ?? 0}</span>
            </span>
            <span>
                <DexIcon dex="Sushiswap" size="20px"/> <span>{dexValues.Sushiswap ?? 0}</span>
            </span>
        </Container>
    );
}
