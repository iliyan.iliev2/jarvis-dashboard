import React from "react"
import { Derivative, derivativeSliceSelector } from "../../state/slices/derivatives";
import { useReduxSelector } from "../../state/useReduxSelector";
import { formatNumberValue } from "../../utils";
import { StatBox } from "../StatBox/StatBox"
import { DexIcon } from "./DexIcon";
import { StatDetails } from "./StatDetails";

export const TotalTradeCountStatBox = ({focusedDerivative}: {focusedDerivative: Derivative}) => {

    let selectDerivative = derivativeSliceSelector(focusedDerivative);
    useReduxSelector(state => Object.values(selectDerivative(state)?.rawDexsData ?? {}));
    const rawDexData = useReduxSelector(state => selectDerivative(state)?.rawDexsData) ?? {};
    const dexTotalTradeCount = useReduxSelector(state => selectDerivative(state)?.dexTotalTradeCount) ?? 0;
    const synthereumTotalTradeCount = focusedDerivative?.totalTradeCount ?? 0;

    return (
        <StatBox
            type="simple-format"
            label="Number of trades"
            value={
                synthereumTotalTradeCount + dexTotalTradeCount
            }
            sourceExplanation={"Obtained from the Synthereum subgraph and dex subgraphs"}
        >
            <StatDetails 
                synthereumValue={formatNumberValue(synthereumTotalTradeCount)}
                dexsTotalValue={formatNumberValue(dexTotalTradeCount)}
                dexValues={
                    {
                        Balancer: rawDexData.Balancer?.totalTradeCount ?? 0,
                        Sushiswap: rawDexData.Sushiswap?.totalTradeCount ?? 0
                    }
                }
            />
        </StatBox>
    );

}