import React from "react"
import { Derivative, derivativeSliceSelector } from "../../state/slices/derivatives";
import { useReduxSelector } from "../../state/useReduxSelector";
import { coMap, sum, TimeWindow } from "../../utils";
import { StatBox } from "../StatBox/StatBox"

export const AverageSpreadStatBox = ({focusedDerivative}: {focusedDerivative: Derivative}) => {

    const TIME_WINDOW = TimeWindow.MONTH;

    let selectDerivative = derivativeSliceSelector(focusedDerivative);
    const windowsUSDPrices = useReduxSelector(state => selectDerivative(state)?.windowsUSDPrices);
    const windowsDexUSDPrices = useReduxSelector(state => selectDerivative(state)?.windowsDexUSDPrices);

    let spreadValues = (windowsUSDPrices === undefined || windowsDexUSDPrices === undefined) ? []
        : coMap(
            windowsUSDPrices[TIME_WINDOW],
            windowsDexUSDPrices[TIME_WINDOW],
            (price, dexPrice) => {
                let priceValue = price.value;
                //use real price if no dex price
                let dexPriceValue = (dexPrice.value !== 0) ? dexPrice.value : priceValue;
                return Math.abs(Number(priceValue) - Number(dexPriceValue))
            }
        );
       

    let averageSpread = sum(spreadValues) / spreadValues.length;

    if(Object.is(NaN, averageSpread)){
        averageSpread = 0;
    }
        
    return (
        <StatBox
            type="valueUSD"
            label="AVG spread (Month)"
            value={averageSpread}
            sourceExplanation="Computed using data from the Synthereum subgraph and dex subgraphs"
        >
        </StatBox>
    );

}