import { combineReducers, configureStore, getDefaultMiddleware, ReducersMapObject } from "@reduxjs/toolkit";
import { isDebounced } from "./debounce";
import { createPersistMiddleware } from "./dev-persist";
import { reducer as app } from './slices/app';
import { reducer as windowsBlocks } from './slices/blocks';
import { reducer as derivativeList } from './slices/derivatives';
import { reducer as jrtData } from './slices/jrt';
import { reducer as windowsSnapshotsData } from './slices/snapshots';
import { reducer as theme } from './slices/theme';
import { reducer as windowsTimestamps } from './slices/time';
import { reducer as windowsTSVData } from './slices/tsv';
import { reducer as windowsTVLData } from './slices/tvl';
import { State } from "./state";


let asyncReducers: ReducersMapObject = {};

function createRootReducer() {
    return combineReducers({
        theme,
        app,
        windowsTimestamps,
        windowsBlocks,
        jrtData,
        windowsTVLData,
        windowsTSVData,
        windowsSnapshotsData,
        derivativeList,
        ...asyncReducers
    });
}
  
export function injectReducers(newAsyncReducers: ReducersMapObject) {
    asyncReducers = {...asyncReducers, ...newAsyncReducers};
    store.replaceReducer(createRootReducer());
}

const persistMiddleware = createPersistMiddleware(['windowsBlocks', /*'windowsSnapshotsData'*/]);

const middleware = [
    ...getDefaultMiddleware({
        immutableCheck: false,
        serializableCheck: false,
        /*thunk: {
            extraArgument: isDebounced,
            
        }*/
    }), 

    //debounceHeavyThunkActionsMiddleware,
    ... persistMiddleware ? [persistMiddleware.middleware] : []
];


const store = configureStore({ 
    reducer: createRootReducer(), 
    middleware,
    preloadedState: persistMiddleware?.getPreloadedState()
});

export default store;
export type ThunkApiConfig = {state: State, extra: typeof isDebounced};





