import { Middleware } from "@reduxjs/toolkit";
import { getSynthereumSnapshotsForTimeWindow } from "./slices/derivatives";

const DEFAULT_DEBOUNCE_PERIOD = 1000;

const SLOW_ACTION_TYPE_PREFIX_TO_DEBOUNCE_TIME_MILLIS = new Map<string, number>([
    [
        getSynthereumSnapshotsForTimeWindow.typePrefix,
        2_000 //there is no reason for the fetched data to change (except for graph redeploy)
    ]
]);


const SLOW_ACTION_TYPE_PREFIXES_TO_PREVIOUS_OCURRENCE_TIMESTAMP = new Map<string, number>();

const REQUEST_ID_TO_DEBOUNCED = new Map<string, boolean>();


export function isDebounced(requestId: string){
    return REQUEST_ID_TO_DEBOUNCED.get(requestId) ?? false;
}

export const debounceHeavyThunkActionsMiddleware: Middleware = () => (dispatch) => (action: {type: string, requestId?: any} & Record<string, any>) => {

    let currentTimestamp = Date.now();

    const heavyActionPrefix = 
        Array.from(SLOW_ACTION_TYPE_PREFIX_TO_DEBOUNCE_TIME_MILLIS.keys())
        .find(prefix => action.type.startsWith(prefix));


    if(heavyActionPrefix === undefined || action.type.endsWith('/rejected')){
        return dispatch(action);
    }

    if(action.type.endsWith('/fulfilled')){
        SLOW_ACTION_TYPE_PREFIXES_TO_PREVIOUS_OCURRENCE_TIMESTAMP.set(heavyActionPrefix, currentTimestamp);
        //console.debug('update prev ocurrence timestamp for', heavyActionPrefix, ':', currentTimestamp);
        return dispatch(action);
    }

    const prevOccurenceTimestamp = 
        SLOW_ACTION_TYPE_PREFIXES_TO_PREVIOUS_OCURRENCE_TIMESTAMP.get(heavyActionPrefix) || (- Infinity);

    const debouncePeriod = 
        SLOW_ACTION_TYPE_PREFIX_TO_DEBOUNCE_TIME_MILLIS.get(heavyActionPrefix) || DEFAULT_DEBOUNCE_PERIOD;
    
    let debounced = false;
    if((currentTimestamp - prevOccurenceTimestamp) < debouncePeriod){
        debounced = true;
        //console.debug('debounced action', action.type);
    }

    REQUEST_ID_TO_DEBOUNCED.set(action.requestId, debounced)

    return dispatch(action);
}
