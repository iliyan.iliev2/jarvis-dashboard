import { createSlice } from '@reduxjs/toolkit';
import { makeTimestampsForWindow, TimeWindow } from '../../utils';

export type WindowTimestampsType = {
    [key in TimeWindow]: number[]
}


interface SetWindowsDataAction {
  payload: WindowTimestampsType
}

const INITIAL_TIME_DATA: WindowTimestampsType = getCurrentTimestamps();

export function getCurrentTimestamps(){
  let windowsData: Partial<WindowTimestampsType> = {};
  for(let time_window of Object.values(TimeWindow)){
      windowsData[time_window as TimeWindow] = makeTimestampsForWindow({ 
          time_window: time_window as TimeWindow
      });
  }
  return windowsData as WindowTimestampsType;
}

const timeDataSlice = createSlice({
  name: 'timeData',
  initialState: INITIAL_TIME_DATA as WindowTimestampsType,
  reducers: {
    setTimeWindowsData(state, action: SetWindowsDataAction) {
      return {...action.payload};
    },
  },
});

export const { setTimeWindowsData: setWindowsData } = timeDataSlice.actions;
export const { reducer } = timeDataSlice;
