import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getSynthereumTVL } from "../../apis/synthereum-subgraph";
import { TimestampedValue, TimeWindow } from "../../utils";
import { ThunkApiConfig } from "../store";
import { getSynthereumSnapshotsForTimeWindow } from "./derivatives";
import { findClosestSnapshot } from "./snapshots";

export type SetTVLValuesForWindowAction = {
  payload: {
    window: TimeWindow,
    values: TimestampedValue[]
  }
}

export type SetCurrentTVLAction = {
    payload: {
      value: number
    }
  }
  
export type WindowsTVLValuesData = {
    currentTVL: number,
    historical: {
        [key in TimeWindow]: TimestampedValue[]
    }
}

const INITIAL_TVL_DATA: WindowsTVLValuesData = {
    currentTVL: 0,
    historical: {
        [TimeWindow.DAY]: [],
        [TimeWindow.WEEK]: [],
        [TimeWindow.MONTH]: [],
        [TimeWindow.YEAR]: [],
        [TimeWindow.ALL_TIME]: [],
    }
}
  
const SLICE_NAME = "TVLData"

export const getCurrentSynthereumTVL  = 
    createAsyncThunk<unknown, undefined, ThunkApiConfig>(
        SLICE_NAME + '/getCurrentSynthereumTVL',
        async (_, { dispatch, getState }) => {
            let currentTvl = await getSynthereumTVL()
            dispatch(setCurrentTVL({value: currentTvl}));
        }
    );

export const getSynthereumTVLForTimeWindow  = 
    createAsyncThunk<unknown, TimeWindow, ThunkApiConfig>(
        SLICE_NAME + '/getSynthereumTVLForTimeWindow',
        async (timeWindow, { dispatch, getState }) => {

            const state = getState();

            const timestamps = state.windowsTimestamps[timeWindow];
            await dispatch(getSynthereumSnapshotsForTimeWindow(timeWindow));

            const snapshots = getState().windowsSnapshotsData[timeWindow];
            const tvlValues = timestamps.map(timestamp => {
                let closestSnapshot = findClosestSnapshot(snapshots, timestamp).snapshot;
                
                return {
                    unixTimestamp: timestamp,
                    value: Number(closestSnapshot.totalValueLockedUSD)
                };
            })

            dispatch(setTVLValuesForWindow({
                values: tvlValues,
                window: timeWindow
            }));
        }
    );

const slice = createSlice({
    name: SLICE_NAME,
    initialState: INITIAL_TVL_DATA,
    reducers: {
        setTVLValuesForWindow(state, action: SetTVLValuesForWindowAction) {
            return {
                ...state,
                historical: {
                    ...state.historical,   
                    [action.payload.window]: action.payload.values
                }
            };
        },
        setCurrentTVL(state, action: SetCurrentTVLAction) {
            return {
                ...state,
                currentTVL: action.payload.value
            };
        },
    },
});


export const { setTVLValuesForWindow, setCurrentTVL } = slice.actions;
export const { reducer } = slice;
