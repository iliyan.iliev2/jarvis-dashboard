import { createSlice } from '@reduxjs/toolkit';
import { ThemeNameType as JarvisThemeNameType } from '@jarvis-network/ui';
import { backgroundMap } from '../../data/theme';

export type ThemeNameType = JarvisThemeNameType;
export type ThemeType = {
  readonly name: ThemeNameType,
  readonly backgroundUrl: string
}


interface SetThemeAction {
  payload: {
    theme: ThemeNameType
  };
}

const INITIAL_THEME: ThemeType = {
  name: 'night',
  backgroundUrl: getThemeBackgroundUrl('night')
};


function getThemeBackgroundUrl(theme: ThemeNameType): string {
  return backgroundMap[theme];
}

const themeSlice = createSlice({
  name: 'theme',
  initialState: INITIAL_THEME as ThemeType,
  reducers: {
    setTheme(state, action: SetThemeAction) {
      return {
        name: action.payload.theme,
        backgroundUrl: getThemeBackgroundUrl(action.payload.theme)
      }
    },
  },
});

export const { setTheme } = themeSlice.actions;
export const { reducer } = themeSlice;
