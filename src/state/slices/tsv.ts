import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { TimestampedValue, TimeWindow } from "../../utils";
import { getAllDerivativeSlicesFromState } from "../state";
import { ThunkApiConfig } from "../store";
import { getAllDerivativesPricesForTimeWindow, getAllDerivativeSupplyAndTradeDataForTimeWindow } from "./derivatives";

//TSV = Total synthetic value

export type SetTSVValuesForWindowAction = {
  payload: {
    window: TimeWindow,
    values: TimestampedValue[]
  }
}

export type WindowsTSVValuesData = {
    [key in TimeWindow]: TimestampedValue[]
}

const INITIAL_TSV_DATA: WindowsTSVValuesData = {
    [TimeWindow.DAY]: [],
    [TimeWindow.WEEK]: [],
    [TimeWindow.MONTH]: [],
    [TimeWindow.YEAR]: [],
    [TimeWindow.ALL_TIME]: [],
}
  
const SLICE_NAME = "TSVData"

export const getSynthereumTSVForTimeWindow  = 
    createAsyncThunk<unknown, TimeWindow, ThunkApiConfig>(
        SLICE_NAME + '/getSynthereumTSVForTimeWindow',
        async (timeWindow, { dispatch, getState, rejectWithValue }) => {

            const state = getState();
            const timestamps = state.windowsTimestamps[timeWindow];

            if(state.derivativeList.length === 0 || state.windowsBlocks[timeWindow].length === 0){
                return;
            }

            
            await Promise.all([
                dispatch(getAllDerivativeSupplyAndTradeDataForTimeWindow({timeWindow})),
                dispatch(getAllDerivativesPricesForTimeWindow({timeWindow}))
            ]);

            const derivativeSlices = getAllDerivativeSlicesFromState(getState());
            const allTotalValues = derivativeSlices.map(derivative => derivative.windowsTotalValues[timeWindow]);

            //CHECK
            const errors: string[] = [];
            for(let totalValues of allTotalValues){
                if(totalValues.length !== timestamps.length){
                    errors.push(`Invalid values' length for a a value list : ${totalValues.length}`)
                }
            }

            if(errors.length !== 0) return rejectWithValue(errors.join('\n'));
            ////

            dispatch(setTSVValuesForWindow({
                values: timestamps.map(t => {
                    return ({
                        unixTimestamp: t,
                        value: allTotalValues
                            .map(values => values.find(value => value.unixTimestamp === t) as TimestampedValue)
                            .map(valuesAtTimestamp => valuesAtTimestamp.value)
                            .reduce((a, b) => Number(a) + Number(b), 0)
                    })
                }),
                window: timeWindow
            }));
        }
    );

const slice = createSlice({
    name: SLICE_NAME,
    initialState: INITIAL_TSV_DATA,
    reducers: {
        setTSVValuesForWindow(state, action: SetTSVValuesForWindowAction) {
            return {
                ...state,
                [action.payload.window]: action.payload.values
            };
        },
    },
});


export const { setTSVValuesForWindow } = slice.actions;
export const { reducer } = slice;
