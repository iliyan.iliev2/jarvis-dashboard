import { createAsyncThunk, createSlice, Reducer } from "@reduxjs/toolkit";
import { getAllPoolsForToken, getBalancerTokenPriceAtBlocks, getPoolDataAtBlocks } from "../../apis/balancer-subgraph";
import { community_subgraph_getLastPriceForAsset, community_subgraph_getPricesForAssetAtTimestamps, getBatchedPricesAtBlocks } from "../../apis/chainlink";
import { Block } from "../../apis/common";
import { getTokenHolderCounts } from "../../apis/synthereum-holder-tracking-subgraph";
import { getDerivatives, getSynthereumSnapshotsAtTimestamps, SynthereumSnapshot } from "../../apis/synthereum-subgraph";
import { getExplodedTokenInfoByBlocks } from "../../apis/uniswapV2";
import { absDistance, completeEachOtherStart, getSecondsTimePeriodForWindow, invertDimension, isDefined, sortByTimestamp, sum, TimestampedValue, TimeWindow } from "../../utils";
import { State } from "../state";
import { ThunkApiConfig } from "../store";
import { findClosestSnapshot, setSynthereumSnapshotsForWindow } from "./snapshots";
import { BaseTokenState, createTokenSlice, DexPayloadData, EMPTY_WINDOWS_VALUES, TokenSlice } from "./token-slice";


export type Derivative = {
    address: string,
    symbol: string,
    tokenDecimals: number,
    tokenCurrencyAddress: string,
    imageURL: string,
    collateralBalance: number,
    supply: number,
    priceUSD: number,
    totalValue: number,
    totalTradeCount: number,
    totalFeePaidUSD: number,
    remainingPoolsCollateral: number,
    totalTradedVolumeUSD: number
}
export type DerivativeListType = Derivative[];

const SLICE_NAME = 'derivativeList';
const INITIAL_DERIVATIVE_LIST_STATE: DerivativeListType = []

const slice = createSlice({
    name: SLICE_NAME,
    initialState: INITIAL_DERIVATIVE_LIST_STATE,
    reducers: {
        setDerivatives(state, action: {payload: Derivative[]}){
            return action.payload;
        },
    },
});

export const { setDerivatives } = slice.actions;
export const { reducer } = slice;

const INITIAL_TOKEN_DATA: BaseTokenState = {
    currentPriceUSD: 0,
    currentMarketcapUSD: -1,
    synthereumTotalTradedVolumeUSD: 0,
    //
    rawDexsData: {},
    dexTotalTradedVolumeUSD: 0,
    dexTotalFeesGeneratedUSD: 0,
    windowsDexTotalFeesGeneratedUSDValues: EMPTY_WINDOWS_VALUES(),
    windowsDexTotalTradeCount: EMPTY_WINDOWS_VALUES(),
    windowsDexTotalTradedVolumeUSDValues: EMPTY_WINDOWS_VALUES(),
    windowsDexTradedUSDVolumes: EMPTY_WINDOWS_VALUES(),
    //
    totalTradeCount: 0,
    dexTotalTradeCount: 0,
    holderCount: 0,
    windowsUSDPrices: EMPTY_WINDOWS_VALUES(),
    windowsDexUSDPrices: EMPTY_WINDOWS_VALUES(),
    windowsSupplyValues: EMPTY_WINDOWS_VALUES(),
    windowsTotalValues: EMPTY_WINDOWS_VALUES(),
    windowsTotalTradedTokenVolume: EMPTY_WINDOWS_VALUES(),
    windowsTotalTradedVolumeUSDValues: EMPTY_WINDOWS_VALUES(),
    windowsTradedTokenVolume: EMPTY_WINDOWS_VALUES(),
    windowsTradedUSDVolumes: EMPTY_WINDOWS_VALUES(),
    windowsTotalTradeCount: EMPTY_WINDOWS_VALUES()
}

export function makeDerivativeSliceName(derivative: Derivative | string){
    let symbol = (typeof derivative === 'object') ? derivative.symbol : derivative;
    return symbol + 'Data';
}

export function derivativeSliceSelector(derivative?: Derivative | string): (state: State) => BaseTokenState | undefined {
    return (state: State) => ( 
        derivative ? (state as any)[makeDerivativeSliceName(derivative)] 
        : undefined 
    );
}


export const getAllDerivatives = createAsyncThunk<unknown, {}, {state: State}>(
    SLICE_NAME + '/getAll',
    async (_, { dispatch, getState }) => {

        if(getState().derivativeList.length !== 0){
            return;
        }

        let derivatives = await getDerivatives();
        const USDC_DECIMALS = 6;

        let derivativeList: Derivative[] = await Promise.all(derivatives.map(async d => {
            
            let tokenDecimals = Number(d.tokenCurrency.decimals);
            let supply = Number(d.supply) / (10 ** tokenDecimals);
            let priceUSD = await community_subgraph_getLastPriceForAsset(d.tokenCurrency.symbol as string);
            let totalValue = (priceUSD * supply);

            return {
                tokenCurrencyAddress: String(d.tokenCurrency.id),
                symbol: String(d.tokenCurrency.symbol),
                address: String(d.id),
                tokenDecimals: tokenDecimals,
                imageURL: 'icons/' + String(d.tokenCurrency.symbol).toLowerCase() + '.png',
                collateralBalance: Number(d.collateralBalance) / (10 ** USDC_DECIMALS),
                remainingPoolsCollateral: Number(d.remainingPoolsCollateral) / (10 ** USDC_DECIMALS),
                supply,
                priceUSD,
                totalValue,
                totalTradeCount: Number(d.totalTradeCount),
                totalFeePaidUSD: Number(d.totalFeePaid),
                totalTradedVolumeUSD: Number(d.totalTradedVolumeUSD)
            };
        }));

        //==== add slices : one slice per derivative ====

        const slices = derivativeList
            .map(derivative => createTokenSlice(makeDerivativeSliceName(derivative), INITIAL_TOKEN_DATA));

        slices.forEach(slice => TokenName_To_Actions.set(slice.name.split('Data')[0], slice.actions));

        //dynamic import to prevent circular dependency
        (await import('../store'))
        .injectReducers(Object.fromEntries(
            slices.map(slice => [slice.name, slice.reducer] as [string, Reducer])
        ));

        //
        dispatch(setDerivatives(derivativeList));
    }
);


export const getSynthereumSnapshotsForTimeWindow = 
    createAsyncThunk<unknown, TimeWindow, ThunkApiConfig>(
        'getSynthereumSnapshotsForTimeWindow',
        async (timeWindow, { dispatch, getState, requestId }) => {

            const state = getState();


            const timestamps = state.windowsTimestamps[timeWindow];
            let previousSnapshots = state.windowsSnapshotsData[timeWindow];

            const snapshotIsCloseTo = (timestamp: number) => 
                (snapshot: SynthereumSnapshot) => absDistance(Number(snapshot.timestamp), timestamp) < 200 /*seconds*/;

            const timestampsOfSnapshotsToFetch = timestamps.filter(timestamp => {
              return ! previousSnapshots.some(snapshotIsCloseTo(timestamp))
            });

            let fetchedSnapshots = await getSynthereumSnapshotsAtTimestamps(timestampsOfSnapshotsToFetch, getSecondsTimePeriodForWindow(timeWindow));

            let allSnapshots = [...previousSnapshots, ...fetchedSnapshots];
            let snapshots = timestamps.map(t => findClosestSnapshot(allSnapshots, t).snapshot);

            dispatch(setSynthereumSnapshotsForWindow({
                values: snapshots,
                window: timeWindow
            }));
            
        }
    );


  
export const getAllDerivativeSupplyAndTradeDataForTimeWindow =
    createAsyncThunk<unknown,  {timeWindow: TimeWindow}, ThunkApiConfig>(
        '<derivative>/getAllDerivativeSupplyAndTradeDataForTimeWindow',
        async ({timeWindow }, { dispatch, getState, rejectWithValue }) => {


            if(getState().derivativeList.length === 0){
                return rejectWithValue('No derivatives yet')
            }

            await dispatch(getSynthereumSnapshotsForTimeWindow(timeWindow));

            const state: Readonly<State> = getState();
            const timestamps = state.windowsTimestamps[timeWindow];
            const snapshots = state.windowsSnapshotsData[timeWindow];

            for await(let derivative of state.derivativeList){

                let {symbol, tokenDecimals, tokenCurrencyAddress} = derivative;

                let actionCreators = getActionCreatorsForToken(symbol);

                if(actionCreators === undefined) return rejectWithValue(`token ${symbol} has no slice`);
                



                let alignedSnapshots: SynthereumSnapshot[] = [];
                let minIndex: number = 0;
                for(let timestamp of timestamps){
                    let {snapshot, index} = findClosestSnapshot(snapshots, timestamp, minIndex);
                    alignedSnapshots.push(snapshot);
                    minIndex = index;
                }

                let tradedSynthVolumes = timestamps.map((timestamp, i) => {

                    let derivativeSnapshots = Array.from(alignedSnapshots[i].derivativeSnapshots);
                    let derivativeSnapshot = derivativeSnapshots.find((d: any) => d.tokenCurrencyAddress === tokenCurrencyAddress);
            
                    return {
                        unixTimestamp: timestamp,
                        value: Number(derivativeSnapshot?.totalTradedSynthVolume ?? 0.01)
                    };
            
                });

                let tradedVolumesUSD: TimestampedValue[] = timestamps.map((timestamp, i) => {
            
                    let derivativeSnapshots = Array.from(alignedSnapshots[i].derivativeSnapshots);
                    let derivativeSnapshot = derivativeSnapshots.find((d: any) => d.tokenCurrencyAddress === tokenCurrencyAddress);
            
                    return {
                        unixTimestamp: timestamp,
                        value: Number(derivativeSnapshot?.totalTradedVolumeUSD ?? 0.01),
                        numberType: 'valueUSD'
                    };
            
                });

                let tradeCountValues = timestamps.map((timestamp, i) => {

            
                    let derivativeSnapshots = Array.from(alignedSnapshots[i].derivativeSnapshots);
                    let derivativeSnapshot = derivativeSnapshots.find((d: any) => d.tokenCurrencyAddress === tokenCurrencyAddress);
            
                    return {
                        unixTimestamp: timestamp,
                        value: Number(derivativeSnapshot?.totalTradeCount ?? 0)
                    };
            
                });


                let supplyValues = timestamps.map((timestamp, i) => {

                    let derivativeSnapshots = Array.from(alignedSnapshots[i].derivativeSnapshots);
                    let derivativeSnapshot: any = derivativeSnapshots.find((d: any) => d.tokenCurrencyAddress === tokenCurrencyAddress);
            
                    return {
                        unixTimestamp: timestamp,
                        value: derivativeSnapshot?.supply ?? 0.01
                    };
            
                });

                dispatch(actionCreators.setSupplyAndTradeDataForWindow({
                    window: timeWindow,
                    totalTradeTokenVolumes: tradedSynthVolumes.map(e => ({
                        unixTimestamp: e.unixTimestamp,
                        value: e.value / (10 ** tokenDecimals)
                    })),
                    totalTradedVolumeUSDValues: tradedVolumesUSD,
                    totalTradeCountValues: tradeCountValues,
                    supplyValues: supplyValues.map(e => ({
                        unixTimestamp: e.unixTimestamp,
                        value: e.value / (10 ** tokenDecimals)
                    }))
                }));
            }
        }
    );


export const getAllDerivativesPricesForTimeWindow =
    createAsyncThunk<unknown,  {timeWindow: TimeWindow}, {state: State}>(
        '<derivative>/getAllDerivativesPricesForTimeWindow',
        async ({timeWindow }, { dispatch, getState, rejectWithValue }) => {

            const state: Readonly<State> = getState();

            if(state.derivativeList.length === 0){
                return rejectWithValue('No derivatives yet')
            }
            
            await Promise.all(state.derivativeList.map(async derivative => {
                let {symbol, tokenCurrencyAddress} = derivative;

                let actionCreators = getActionCreatorsForToken(symbol);
                if(actionCreators === undefined) return rejectWithValue(`token ${symbol} has no slice`);
            
                const timestamps = state.windowsTimestamps[timeWindow];
                const blocks = state.windowsBlocks[timeWindow];
                const snapshots = state.windowsSnapshotsData[timeWindow];
    
                let prices = await _getDerivativePricesForTimeWindow({
                    timestamps, blocks, snapshots, symbol, tokenCurrencyAddress
                });
    
                     
                dispatch(actionCreators.setPricesForWindow({
                    window: timeWindow,
                    prices: prices
                }));
            }));
        }
    );


export const getAllDerivativesHolderCount = createAsyncThunk<unknown, undefined, {state: State}>(
    '<derivative>/getAllDerivativesHolderCount',
    async (_, { dispatch, getState, rejectWithValue }) => {
    
        

        const state = getState();

        if(state.derivativeList.length === 0){
            return rejectWithValue('No derivatives yet')
        }
        

        let holderCounts = await getTokenHolderCounts(state.derivativeList.map(d => d.tokenCurrencyAddress));

        for(let tokenAddress in holderCounts){
            let holderCount = holderCounts[tokenAddress];
            let symbol = state.derivativeList.find(d => d.tokenCurrencyAddress === tokenAddress)?.symbol as string;
            let actionCreators = getActionCreatorsForToken(symbol);

            if(actionCreators === undefined){
                if(actionCreators === undefined) return rejectWithValue(`token ${symbol} has no slice`);
            }
            dispatch(actionCreators.setHolderCount({holderCount}))
        }
           
    }
);


const _getDerivativePricesForTimeWindow = async(
    {symbol, timestamps, snapshots, blocks, tokenCurrencyAddress}: {
        tokenCurrencyAddress: string, 
        symbol: string, 
        timestamps: number[], 
        snapshots: SynthereumSnapshot[],
        blocks: Block[]
    }
) => {
    let prices: TimestampedValue[];
    if('using deprecated Chainlink API subgraph'){
        prices = await community_subgraph_getPricesForAssetAtTimestamps(symbol, timestamps);
    } else {
        //other method: calling the new chainlink API contracts with a web3 provider (archival node)
        //too slow: needs better optimization if possible

        prices = timestamps.map((timestamp, i) => {

            let closestSnapshot = findClosestSnapshot(snapshots, timestamp).snapshot;
    
            if(absDistance(Number(closestSnapshot.timestamp), timestamp) > 600){
                return undefined;
            }

            let derivativeSnapshots = Array.from(closestSnapshot.derivativeSnapshots);
            let derivativeSnapshot = derivativeSnapshots.find((d: any) => d.tokenCurrencyAddress === tokenCurrencyAddress);
    
            return {
                unixTimestamp: timestamp,
                value: Number(derivativeSnapshot?.priceUSD ?? 0.01)
            };

        }).filter(isDefined);

        let timestampsOfMissingPrices = 
            timestamps.filter(t => ! prices.some(price => price.unixTimestamp === t))
            .map((timestamp, index) => ({timestamp, index}));

        let blocksOfMissingPrices = timestampsOfMissingPrices.map(({index}) => blocks[index]);
        
        let missingPrices: TimestampedValue[] = 
            (await getBatchedPricesAtBlocks(symbol, blocksOfMissingPrices.map(block => block.number)))
            .map((price, i) => ({value: price, unixTimestamp: timestampsOfMissingPrices[i].timestamp}))

        
        prices = [...prices, ...missingPrices].sort(sortByTimestamp);
    }
    return prices;
}

export const getDerivativePricesForTimeWindow =
    createAsyncThunk<unknown,  {timeWindow: TimeWindow, tokenCurrencyAddress: string, symbol: string}, {state: State}>(
        '<derivative>/getDerivativePricesForTimeWindow',
        async ({timeWindow, tokenCurrencyAddress, symbol}, { dispatch, getState, rejectWithValue }) => {

            const state: Readonly<State> = getState();

            if(state.derivativeList.length === 0){
                return rejectWithValue('No derivatives yet')
            }
            
            const timestamps = state.windowsTimestamps[timeWindow];
            const blocks = state.windowsBlocks[timeWindow];
            const snapshots = state.windowsSnapshotsData[timeWindow];

            let actionCreators = getActionCreatorsForToken(symbol);
            if(actionCreators === undefined) return rejectWithValue(`token ${symbol} has no slice`);
        

            let prices = await _getDerivativePricesForTimeWindow({timestamps, blocks, snapshots, symbol, tokenCurrencyAddress});
        
            dispatch(actionCreators.setPricesForWindow({
                window: timeWindow,
                prices: prices
            }));
        }
    );
   
    
export const getSynthTokenDexInfoForTimeWindow = createAsyncThunk<unknown, {tokenCurrencyAddress: string, tokenSymbol: string, timeWindow: TimeWindow}, ThunkApiConfig>(
    '<derivative>/getSynthTokenDexInfoForTimeWindow',
    async ({tokenCurrencyAddress, timeWindow, tokenSymbol}, { dispatch, getState, rejectWithValue }) => {
  
        const blocks = getState().windowsBlocks[timeWindow];

        if(blocks.length === 0) return rejectWithValue('No blocks');
        
        let actionCreators = getActionCreatorsForToken(tokenSymbol);
        if(actionCreators === undefined) return rejectWithValue(`token ${tokenSymbol} has no slice`);


        //SUSHISWAP
        let sushiswapPayloadData: DexPayloadData | undefined;

        let sushiswapPromise = (async() => {
            let {
                prices,
                totalVolumeUSDValues,
                totalFeesGeneratedUSDValues,
                totalTradeCounts ,
            } = await getExplodedTokenInfoByBlocks({tokenAddress: tokenCurrencyAddress, blocks, sushiswap: true})


            sushiswapPayloadData = {
                prices,
                totalTradedVolumeUSD: totalVolumeUSDValues,
                totalFeesGeneratedUSD: totalFeesGeneratedUSDValues,
                totalTradeCounts
            };
        })();

        //BALANCER
        let balancerPayloadData: DexPayloadData | undefined;

        let balancerPromise = Promise.all([
            (async() => {
                let pools = await getAllPoolsForToken(tokenSymbol);
    
                let poolsData = await Promise.all(
                    pools.map(pool => getPoolDataAtBlocks({blocks, poolId: String(pool.id)}))
                );
        
                let invertedPoolsData = invertDimension(poolsData);
        
                balancerPayloadData = {
                    prices: [],
                    ... balancerPayloadData,
                    totalTradeCounts: invertedPoolsData.map(poolsData => ({
                        unixTimestamp: poolsData[0].unixTimestamp,
                        value: sum(poolsData.map(poolData => poolData.swapsCount))
                    })),
                    totalTradedVolumeUSD:  invertedPoolsData.map(poolsData => ({
                        unixTimestamp: poolsData[0].unixTimestamp,
                        value: sum(poolsData.map(poolData => poolData.totalSwapVolume))
                    })),
                    totalFeesGeneratedUSD:  invertedPoolsData.map(poolsData => ({
                        unixTimestamp: poolsData[0].unixTimestamp,
                        value: sum(poolsData.map(poolData => poolData.totalSwapVolume * poolData.swapFee))
                    }))
                }
                 
            
            })(),

            (async() => {
                balancerPayloadData = {
                    totalFeesGeneratedUSD: [],
                    totalTradeCounts: [],
                    totalTradedVolumeUSD: [],
                    ... balancerPayloadData,
                    prices: await getBalancerTokenPriceAtBlocks({blocks, tokenAddress: tokenCurrencyAddress, timeWindow})
                }
            })()
        ]);

        await Promise.all([sushiswapPromise, balancerPromise]);


        if(sushiswapPayloadData === undefined || balancerPayloadData === undefined){
            return rejectWithValue('');
        }

        //remove last price if balancer prices has extra element.
        if(balancerPayloadData.prices.length === sushiswapPayloadData.prices.length + 1){
            balancerPayloadData.prices.pop();
        }

        completeEachOtherStart([
            sushiswapPayloadData.prices,
            balancerPayloadData.prices
        ].filter(arr => arr.length !== 0));

        dispatch(actionCreators.setDexDataForWindow({
            window: timeWindow,
            dexsData: {
                Sushiswap: sushiswapPayloadData,
                Balancer: balancerPayloadData,
            }
        }));
    }
  );

const TokenName_To_Actions = new Map<string, TokenSlice['actions']>();

export function getActionCreatorsForToken(tokenName: string){
    return TokenName_To_Actions.get(tokenName);
}
