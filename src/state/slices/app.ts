import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { TokenDataKind } from "../../components/TokenAreaChart/TokenAreaChart";
import { ThunkApiConfig } from "../store";
import { Derivative, getAllDerivativesHolderCount, getAllDerivatives } from "./derivatives";
import { getJRTHolderCount, getJRTMarketcapFromCoingecko, getJRTPriceUSD } from "./jrt";
import { getCurrentSynthereumTVL } from "./tvl";


const SLICE_NAME = 'app';
export type TabIndex = number;

export type AppType = {
    blocksLoading: boolean,
    firstTabDataLoading: boolean,
    tab: TabIndex,
    focusedDerivative?: Derivative,
    focusedDerivativeData?: TokenDataKind,
}

const INITIAL_APP: AppType = {
    blocksLoading: true,
    firstTabDataLoading: true,
    tab: 0
}

export interface SetTabAction {
    payload: TabIndex;
}

export interface SetFocusedDerivative {
    payload: Derivative | undefined;
}

export interface SetFocusedDerivativeDataKind {
    payload: TokenDataKind | undefined;
}

export const loadFirstTabContent = createAsyncThunk<unknown, undefined, ThunkApiConfig>(
  SLICE_NAME + '/loadFirstTabContent',
  async (_, { dispatch }) => {

    await Promise.all([
        dispatch(getJRTMarketcapFromCoingecko({})),
        dispatch(getJRTPriceUSD()),
        dispatch(getJRTHolderCount()),

        dispatch(getCurrentSynthereumTVL()),
        dispatch(getAllDerivatives({})).then(() => {
            dispatch(getAllDerivativesHolderCount());
        })
    ]);

  }
);

const appSlice = createSlice({
    name: SLICE_NAME,
    initialState: INITIAL_APP,
    reducers: {
        setTab(state, action: SetTabAction) {
            return {
                ...state,
                tab: action.payload
            }
        },
        setAllBlocksLoaded(state, action: {}) {
            return {
                ...state,
                blocksLoading: false
            }
        },       
        setFocusedDerivative(state, action: SetFocusedDerivative) {
            return {
                ...state,
                focusedDerivative: action.payload,
                focusedDerivativeData: 'price'
            }
        },
        setFocusedDerivativeDataKind(state, action: SetFocusedDerivativeDataKind) {
            return {
                ...state,
                focusedDerivativeData: action.payload,
            }
        },
    },

    extraReducers: {
        [loadFirstTabContent.pending as any]: (state) => {
            state.firstTabDataLoading = true;
        },
        [loadFirstTabContent.fulfilled as any]: (state) => {
            state.firstTabDataLoading = false;
        }
    }
});
  

export const {
    setTab,
    setAllBlocksLoaded,
    setFocusedDerivative,
    setFocusedDerivativeDataKind
} = appSlice.actions;

export const { reducer } = appSlice;
