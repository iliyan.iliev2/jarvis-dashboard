import { createAsyncThunk } from '@reduxjs/toolkit';
import { getCurrentJRTMarketCap } from '../../apis/coingecko';
import { JRT_ADDRESS } from '../../apis/data/contract-addresses';
import { getTokenHolderCounts } from '../../apis/synthereum-holder-tracking-subgraph';
import { getETH_USDCPrice, getExplodedTokenInfoByBlocks, getTokenETHPairPrice } from '../../apis/uniswapV2';
import { TimeWindow } from '../../utils';
import { State } from '../state';
import { BaseTokenState, createTokenSlice, EMPTY_WINDOWS_VALUES } from './token-slice';


const INITIAL_TOKEN_DATA: BaseTokenState = {
  currentPriceUSD: 0,
  currentMarketcapUSD: -1,
  synthereumTotalTradedVolumeUSD: 0,
  dexTotalTradedVolumeUSD: 0,
  totalTradeCount: 0,

  windowsDexTotalFeesGeneratedUSDValues: EMPTY_WINDOWS_VALUES(),
  windowsDexTotalTradedVolumeUSDValues: EMPTY_WINDOWS_VALUES(),
  windowsDexTotalTradeCount: EMPTY_WINDOWS_VALUES(),
  windowsDexTradedUSDVolumes: EMPTY_WINDOWS_VALUES(),
  dexTotalTradeCount: 0,
  dexTotalFeesGeneratedUSD: 0,
  rawDexsData: {},

  holderCount: 0,
  windowsUSDPrices: EMPTY_WINDOWS_VALUES(),
  windowsDexUSDPrices: EMPTY_WINDOWS_VALUES(),
  windowsSupplyValues: EMPTY_WINDOWS_VALUES(),
  windowsTotalValues: EMPTY_WINDOWS_VALUES(),
  windowsTotalTradedTokenVolume: EMPTY_WINDOWS_VALUES(),
  windowsTotalTradedVolumeUSDValues: EMPTY_WINDOWS_VALUES(),
  windowsTradedTokenVolume: EMPTY_WINDOWS_VALUES(),
  windowsTradedUSDVolumes: EMPTY_WINDOWS_VALUES(),
  windowsTotalTradeCount: EMPTY_WINDOWS_VALUES(),
}

const JRTDataSlice = createTokenSlice('JRTData', INITIAL_TOKEN_DATA);

export const getJRTMarketcapFromCoingecko = createAsyncThunk<unknown, {}, {state: State}>(
  JRTDataSlice.name + '/getMarketcap',
  async (_, { dispatch }) => {
    let marketcapUSD = await getCurrentJRTMarketCap();
    dispatch(setCurrentMarketcap({ marketcapUSD }));
  }
);


export const getJRTPriceUSD = createAsyncThunk<unknown, undefined, {state: State}>(
  JRTDataSlice.name + '/getPrice',
  async (_, { dispatch }) => {

    let ethPriceUSD = Number(await getETH_USDCPrice());

    let jrtPriceEth_Uniswap = Number(await getTokenETHPairPrice(JRT_ADDRESS, false));
    let jrtPriceEth_Sushiswap = Number(await getTokenETHPairPrice(JRT_ADDRESS, true));

    let priceUSD = ((jrtPriceEth_Uniswap + jrtPriceEth_Sushiswap) / 2) * ethPriceUSD;
    dispatch(setCurrentPrice({ currentPriceUSD: priceUSD }));
  }
);

export const getJRTHolderCount = createAsyncThunk<unknown, undefined, {state: State}>(
  JRTDataSlice.name + '/getJRTHolderCount',
  async (_, { dispatch }) => {

    let {[JRT_ADDRESS]: holderCount} = await getTokenHolderCounts([JRT_ADDRESS]);
    dispatch(setHolderCount({ holderCount: Number(holderCount) }))
  }
);



export const getJRTPriceUSDForTimeWindow = createAsyncThunk<unknown, TimeWindow, {state: State}>(
  JRTDataSlice.name + '/getJRTPriceUSDForTimeWindow',
  async (timeWindow, { dispatch, getState, rejectWithValue }) => {

    const blocks = getState().windowsBlocks[timeWindow];

    if(blocks.length === 0) return rejectWithValue('No blocks');

    const [uniswapInfo, sushiswapInfo] = await Promise.all([
      getExplodedTokenInfoByBlocks({tokenAddress: JRT_ADDRESS, blocks}),
      getExplodedTokenInfoByBlocks({tokenAddress: JRT_ADDRESS, blocks, sushiswap: true})
    ]);

      dispatch(setDexDataForWindow({
        window: timeWindow,

        dexsData: {
          Uniswap: {
            prices: uniswapInfo.prices,
            totalFeesGeneratedUSD: [],
            totalTradeCounts: [],
            totalTradedVolumeUSD: uniswapInfo.totalFeesGeneratedUSDValues
          },
          Sushiswap: {
            prices: sushiswapInfo.prices,
            totalFeesGeneratedUSD: [],
            totalTradeCounts: [],
            totalTradedVolumeUSD: sushiswapInfo.totalFeesGeneratedUSDValues
          }
        }
      }));
  }
);

export const { setPricesForWindow, setCurrentMarketcap, setCurrentPrice, setHolderCount, setDexDataForWindow } = JRTDataSlice.actions;
export const { reducer } = JRTDataSlice;
