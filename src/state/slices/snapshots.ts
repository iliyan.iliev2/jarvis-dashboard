import { createSlice } from "@reduxjs/toolkit";
import { SynthereumSnapshot } from "../../apis/synthereum-subgraph";
import { TimeWindow } from "../../utils";

export type SetSynthereumSnapshotsForWindowAction = {
  payload: {
    window: TimeWindow,
    values: SynthereumSnapshot[]
  }
}

export type WindowsSynthereumSnapshotData = {
    [key in TimeWindow]: SynthereumSnapshot[]
}

const INITIAL_TVL_DATA: WindowsSynthereumSnapshotData = {
    [TimeWindow.DAY]: [],
    [TimeWindow.WEEK]: [],
    [TimeWindow.MONTH]: [],
    [TimeWindow.YEAR]: [],
    [TimeWindow.ALL_TIME]: [],
}
  

const slice = createSlice({
    name: 'TVLData',
    initialState: INITIAL_TVL_DATA,
    reducers: {
        setSynthereumSnapshotsForWindow(state, action: SetSynthereumSnapshotsForWindowAction) {
            return {
                ...state,
                [action.payload.window]: action.payload.values
            };
        },
    },
});

/**
 * @param snapshots - must be sorted (ascending timestmamp)
 */
export function findClosestSnapshot(snapshots: SynthereumSnapshot[], timestamp: number, minIndex: number = 0): {snapshot: SynthereumSnapshot, index: number} {
    let closestSnapshot = snapshots[0];
    let closestSnapshotIndex = 0;
    let closestDelta: number = Math.abs(Number(closestSnapshot.id) - timestamp);
    let delta: number;

    for(let i = minIndex; i < snapshots.length; i ++){
        let snapshot = snapshots[i];

        delta = Math.abs(Number(snapshot.id) - timestamp);

        if(closestDelta >= delta){
            closestSnapshot = snapshot;
            closestSnapshotIndex = i
            closestDelta = delta;
        } else {
            break;
        }
    }
    return {
        snapshot: closestSnapshot,
        index: closestSnapshotIndex
    };
}

export const { setSynthereumSnapshotsForWindow } = slice.actions;
export const { reducer } = slice;
