import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import deepEqual from 'deep-equal';
import { getBlockNumbersByTimestamps } from '../../apis/blocklytics';
import { Block } from '../../apis/common';
import { absDistance, TimeWindow } from '../../utils';
import { State } from '../state';
import { setAllBlocksLoaded } from './app';


type ThunkArg = {timestamps: number[], timeWindow: TimeWindow};

export const getBlocksForTimeWindow = createAsyncThunk<unknown, ThunkArg, {state: State}>(
  'blocks/getBlocksForTimeWindow',
  async ({ timestamps, timeWindow }, { dispatch, getState,  }) => {
    
    const blockCount = getState().windowsBlocks[timeWindow].length;
    if(blockCount !== 0){
      console.debug('getBlocksForTimeWindow: ignored');
      return;
    }

    let blocks = await getBlockNumbersByTimestamps(timestamps);
    dispatch(setWindowBlocksDataAction({window: timeWindow as TimeWindow, blocks}));
  }
);

export const getBlocksForAllTimeWindows = createAsyncThunk<unknown, undefined, {state: State}>(
  'blocks/getBlocksForAllTimeWindows',
  async (_, { dispatch, getState,  }) => {
    
    const state = getState();

    let promises: Promise<any[]>[] = [];

    Object.values(TimeWindow).map(async timeWindow => {
        let previousBlocks = state.windowsBlocks[timeWindow];
        let timestamps = state.windowsTimestamps[timeWindow];

        if(deepEqual(previousBlocks.map(b => b.timestamp), timestamps)){
          return;
        }

        const blockIsCloseTo = (timetamp: number) => (block: Block) => absDistance(block.timestamp, timetamp) < 200 /*seconds*/;

        let timestampsOfBlocksToFetch = timestamps.filter(timestamp => {
          return ! previousBlocks.some(blockIsCloseTo(timestamp))
        });

        let fetchedBlocks = await getBlockNumbersByTimestamps(timestampsOfBlocksToFetch);

        let allBlocks = [...previousBlocks, ...fetchedBlocks];
        let blocks = timestamps.map(t => findClosestBlock(allBlocks, t));
        
        promises.push(
          dispatch(setWindowBlocksDataAction({window: timeWindow as TimeWindow, blocks})) as any
        );
    });
    
    await Promise.all(promises);
    dispatch(setAllBlocksLoaded());
  }
);

export type WindowsBlocksDataType = {
  [key in TimeWindow]: Block[]
}

interface setWindowBlocksDataAction {
  payload: {window: TimeWindow, blocks: Block[]}
}

const INITIAL_BLOCKS_DATA: WindowsBlocksDataType = {
  [TimeWindow.DAY]: [],
  [TimeWindow.WEEK]: [],
  [TimeWindow.MONTH]: [],
  [TimeWindow.YEAR]: [],
  [TimeWindow.ALL_TIME]: [],
}

const windowBlocksDataSlice = createSlice({
  name: 'blocksData',
  initialState: INITIAL_BLOCKS_DATA,
  reducers: {
    setWindowBlocksDataAction(state, action: setWindowBlocksDataAction) {
      return {
        ...state,
        [action.payload.window]: action.payload.blocks
      }
    },
  },
  extraReducers: {
    [getBlocksForAllTimeWindows.pending as any]: (state, action) => {
    },
    [getBlocksForAllTimeWindows.fulfilled as any]: (state, { payload }) => {
    },
    [getBlocksForAllTimeWindows.rejected as any]: (state, action) => {
    },
  }
});

export const { setWindowBlocksDataAction } = windowBlocksDataSlice.actions;
export const { reducer } = windowBlocksDataSlice;


export function findClosestBlock(blocks: Block[], timestamp: number): Block {
  return blocks.reduce((_closestBlock, block) => {
      if(Math.abs(_closestBlock.timestamp - timestamp) > Math.abs(block.timestamp - timestamp)){
          return block;
      }
      return _closestBlock;
  });
}