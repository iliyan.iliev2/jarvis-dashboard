import { createSlice } from "@reduxjs/toolkit";
import { coMap, isDefined, lastOrDefault, sum, TimestampedValue, TimeWindow } from "../../utils";



export type Dex = 'Uniswap' | 'Sushiswap' | 'Balancer'

type SingleRawDexData = {
  totalTradedVolumeUSD: number,
  totalTradeCount: number,
  totalFeesGeneratedUSD: number,
  windowsPrices: WindowsValues,
  windowsTotalTradedCountValues: WindowsValues,
  windowsTotalTradedVolumeUSDValues: WindowsValues,
  windowsTotalFeesGeneratedUSD: WindowsValues,
}

export type RawDexsData = {
  [dex in Dex]?: SingleRawDexData
}

export type DexPayloadData = {
  prices: TimestampedValue[]
  totalTradedVolumeUSD: TimestampedValue[],
  totalTradeCounts: TimestampedValue[],
  totalFeesGeneratedUSD: TimestampedValue[]
}

export type SetDexDataForWindowAction = {
  payload: {
    window: TimeWindow,
    dexsData: Partial<Record<Dex, DexPayloadData>>
  }
}

export type SetPricesForWindowAction = {
  payload: {
    window: TimeWindow,
    prices: TimestampedValue[]
  }
}

export type SetCurrentMarketcapAction = {
  payload: {
    marketcapUSD: number
  }
}

export type SetHolderCountAction = {
  payload: {
    holderCount: number
  }
}

export type SetCurrentPriceAction = {
  payload: {
    currentPriceUSD: number
  }
}


export type SetSupplyAndTradeDataForWindowAction = {
  payload: {
    window: TimeWindow,
    supplyValues: TimestampedValue[],
    totalTradeTokenVolumes: TimestampedValue[],
    totalTradedVolumeUSDValues: TimestampedValue[],
    totalTradeCountValues: TimestampedValue[],
  }
}



type WindowsValues = {
  [key in TimeWindow]: TimestampedValue[]
};



export type BaseTokenState = {
  currentPriceUSD: number,
  currentMarketcapUSD: number,
  holderCount: number,
  //
  rawDexsData: RawDexsData,
  //dex aggregated data
  dexTotalTradedVolumeUSD: number,
  dexTotalTradeCount: number,
  dexTotalFeesGeneratedUSD: number,
  windowsDexTotalFeesGeneratedUSDValues: WindowsValues,
  windowsDexTotalTradedVolumeUSDValues: WindowsValues,
  windowsDexTotalTradeCount: WindowsValues,
  windowsDexTradedUSDVolumes: WindowsValues,
  //synthereum data
  synthereumTotalTradedVolumeUSD: number,
  totalTradeCount: number,
  windowsDexUSDPrices: WindowsValues,
  windowsUSDPrices: WindowsValues,
  windowsSupplyValues: WindowsValues,
  windowsTotalValues: WindowsValues,
  windowsTotalTradedTokenVolume: WindowsValues,
  windowsTradedTokenVolume: WindowsValues,
  windowsTotalTradedVolumeUSDValues: WindowsValues,
  windowsTradedUSDVolumes: WindowsValues,
  windowsTotalTradeCount: WindowsValues,
}

export const EMPTY_WINDOWS_VALUES = () => ({
  [TimeWindow.DAY]: [],
  [TimeWindow.WEEK]: [],
  [TimeWindow.MONTH]: [],
  [TimeWindow.YEAR]: [],
  [TimeWindow.ALL_TIME]: [],
})

export function createTokenSlice<
  Name extends string,
  InitialState extends BaseTokenState
>(name: Name, initialState: InitialState){

  //(Pure!) util: update computed data
  const postReduce = (timeWindow: TimeWindow, state: BaseTokenState): BaseTokenState => {

    const prices = state.windowsUSDPrices[timeWindow];
    const windowsTotalTradedVolumeUSDValues = state.windowsTotalTradedVolumeUSDValues[timeWindow];

    const windowsTradedTokenVolumes = 
      state.windowsTotalTradedTokenVolume[timeWindow].reduce((result, totalTradedVolume, i, array) => {
        let prevTotalTradedVolume: TimestampedValue | undefined = array[i - 1];
        result.push({
          unixTimestamp: totalTradedVolume.unixTimestamp,
          value: Number(totalTradedVolume.value) - Number(prevTotalTradedVolume?.value ?? 0),
          numberType: 'simple-format'
        })
        return result;

      }, [] as TimestampedValue[]);

          
    const windowsTradedUSDVolumes = 
      windowsTotalTradedVolumeUSDValues.reduce((result, totalTradedVolume, i, array) => {
        let prevTotalTradedVolume: TimestampedValue | undefined = array[i - 1];
        result.push({
          unixTimestamp: totalTradedVolume.unixTimestamp,
          value: Number(totalTradedVolume.value) - Number(prevTotalTradedVolume?.value ?? 0),
          numberType: 'valueUSD'
        })
        return result;

      }, [] as TimestampedValue[]);

    const synthereumTotalTradedVolumeUSD = Number(
      lastOrDefault(
        windowsTotalTradedVolumeUSDValues.map(e => e.value), 
        state.synthereumTotalTradedVolumeUSD ?? 0
      )
    );


    return {
      ...state,
      synthereumTotalTradedVolumeUSD: synthereumTotalTradedVolumeUSD,

      windowsTotalValues: {
        ... state.windowsTotalValues,
        [timeWindow]: coMap(
          state.windowsSupplyValues[timeWindow], prices,
          (supply, price): TimestampedValue => ({
            unixTimestamp: supply.unixTimestamp, 
            value: Number(supply.value) * Number(price.value),
            numberType: 'simple-format'
          })
        )
      },

      windowsTotalTradedVolumeUSDValues: {
        ... state.windowsTotalTradedVolumeUSDValues,
        [timeWindow]: windowsTotalTradedVolumeUSDValues
      },

      windowsTradedTokenVolume: {
        ... state.windowsTradedTokenVolume,
        [timeWindow]: windowsTradedTokenVolumes
      },

      windowsTradedUSDVolumes: {
        ... state.windowsTradedUSDVolumes,
        [timeWindow]: windowsTradedUSDVolumes
      }
    }
  }


  const dexPostReduce = (timeWindow: TimeWindow, state: BaseTokenState): BaseTokenState => {

    let windowDexsData = Object.entries(state.rawDexsData)
        .filter(([dex, data]) => isDefined(data))
        .map(([dex, data]) => ({
          prices: (data as SingleRawDexData).windowsPrices[timeWindow],
          volumes: (data as SingleRawDexData).windowsTotalTradedVolumeUSDValues[timeWindow],
          tradeCount: (data as SingleRawDexData).windowsTotalTradedCountValues[timeWindow],
          feesGenerated: (data as SingleRawDexData).windowsTotalFeesGeneratedUSD[timeWindow],
        }));

    let dexVolumes = windowDexsData.map(e => e.volumes);
    let dexsTradeCounts = windowDexsData.map(e => e.tradeCount);
    let dexFeesGenerated = windowDexsData.map(e => e.feesGenerated);
    let dexsPrices = windowDexsData.map(e => e.prices);
    //
    let windowTotalTradedVolumeUSDValues: TimestampedValue[] = [];
    let windowTotalFeesGeneratedUSD: TimestampedValue[] = [];
    let windowTotalTradedCount: TimestampedValue[] = [];
    let windowUSDPrices: TimestampedValue[] = [];
    let priceWeights: number[][] = [];

    for(let i = 0; i < dexVolumes[0].length; i++){
      let timestamp = dexVolumes[0][i].unixTimestamp;
      let volumes = dexVolumes.map(volumes => Number(volumes[i].value));
      if(! volumes.every(isDefined)){
        throw new Error(`volume at index ${i} not defined`)
      }
      let totalVolume = sum(volumes)
      if(totalVolume === 0){
        totalVolume = Number.EPSILON;
      }

      /*
        the price weight of a dex is equal to its volume dominance at a given timestamp.
        Gor example if 90% of the dex volume happens on sushiswap at a timestamp t the price weight 
        of sushiswap will be 0.9
      */
      priceWeights.push( volumes.map(volume => volume / totalVolume) );

      windowTotalTradedVolumeUSDValues.push({
        unixTimestamp: timestamp,
        value: totalVolume,
        numberType: 'valueUSD'
      });
    }

    for(let i = 0; i < dexsTradeCounts[0].length; i++){
      let timestamp = dexsTradeCounts[0][i].unixTimestamp;

      windowTotalTradedCount.push({
        unixTimestamp: timestamp,
        value: sum( 
          dexsTradeCounts.map(tradeCounts => Number(tradeCounts[i].value))
        ),
        numberType: 'simple-format'
      });
    }

    for(let i = 0; i < dexsPrices[0].length; i++){
      let timestamp = dexsPrices[0][i].unixTimestamp;

      windowUSDPrices.push({
        unixTimestamp: timestamp,
        value: sum( 
          dexsPrices.map((price, dexNumber) => {
            let weight = priceWeights[i][dexNumber];
            let priceAtIndex = price[i].value;
            return weight * Number(priceAtIndex);
          })
        ),
        numberType: 'valueUSD'
      });
    }

    for(let i = 0; i < dexFeesGenerated[0].length; i++){
      let timestamp = dexFeesGenerated[0][i].unixTimestamp;

      windowTotalFeesGeneratedUSD.push({
        unixTimestamp: timestamp,
        value: sum( 
          dexFeesGenerated.map(feesGenerated => Number(feesGenerated[i].value))
        ),
        numberType: 'valueUSD'
      });
    }

    const windowsTradedUSDVolumes = 
      windowTotalTradedVolumeUSDValues.reduce((result, totalTradedVolume, i, array) => {
        let prevTotalTradedVolume: TimestampedValue | undefined = array[i - 1];
        result.push({
          unixTimestamp: totalTradedVolume.unixTimestamp,
          value: Number(totalTradedVolume.value) - Number(prevTotalTradedVolume?.value ?? 0),
          numberType: 'valueUSD'
        })
        return result;

      }, [] as TimestampedValue[]);


    return {
      ...state,

      windowsDexUSDPrices: {
        ...state.windowsDexUSDPrices,
        [timeWindow]: windowUSDPrices
      },

      windowsDexTotalTradedVolumeUSDValues: {
        ...state.windowsDexTotalTradedVolumeUSDValues,
        [timeWindow]: windowTotalTradedVolumeUSDValues
      },

      windowsDexTradedUSDVolumes: {
        ...state.windowsDexTradedUSDVolumes,
        [timeWindow]: windowsTradedUSDVolumes
      },

      windowsDexTotalFeesGeneratedUSDValues: {
        ...state.windowsDexTotalFeesGeneratedUSDValues,
        [timeWindow]: windowTotalFeesGeneratedUSD
      },

      windowsDexTotalTradeCount: {
        ...state.windowsDexTotalTradeCount,
        [timeWindow]: windowTotalTradedCount
      },

      dexTotalTradedVolumeUSD: Number(
        lastOrDefault(windowTotalTradedVolumeUSDValues, {unixTimestamp: 0, value: 0}).value
      ),

      dexTotalTradeCount: Number(
        lastOrDefault(windowTotalTradedCount, {unixTimestamp: 0, value: 0}).value
      ),

      dexTotalFeesGeneratedUSD: Number(
        lastOrDefault(windowTotalFeesGeneratedUSD, {unixTimestamp: 0, value: 0}).value
      )
    };
  };

  const slice = createSlice({
    name,
    initialState: initialState as BaseTokenState,
    reducers: {
      setCurrentPrice(state, action: SetCurrentPriceAction){
        return {
          ...state,
          currentPriceUSD: action.payload.currentPriceUSD
        }
      },
      setCurrentMarketcap(state, action: SetCurrentMarketcapAction){
        return {
          ...state,
          currentMarketcapUSD: action.payload.marketcapUSD
        }
      },
      setHolderCount(state, action: SetHolderCountAction){
        return {
          ...state,
          holderCount: action.payload.holderCount
        }
      },
      setPricesForWindow(state, action: SetPricesForWindowAction) {
        return postReduce(action.payload.window, {
          ...state,

          windowsUSDPrices: {
            ... state.windowsUSDPrices,
            [action.payload.window]: action.payload.prices.map(price => ({...price, numberType: 'valueUSD'}))
          },
        });
      },
      setDexDataForWindow(state, action: SetDexDataForWindowAction) {

        Object.entries(action.payload.dexsData).forEach(([dex, data]) => {
          if(data?.prices.length === 0 || data?.totalTradedVolumeUSD.length === 0){
            action = {...action};
            delete action.payload.dexsData[dex as Dex];
          }
        });

        return dexPostReduce(action.payload.window, {
          ...state,
          rawDexsData: {
            ...state.rawDexsData,

            ... Object.fromEntries(Object.entries(action.payload.dexsData).map(([dex, data]) => {
              const alreadyPresentDexData = state.rawDexsData[dex as Dex];

              return [dex, {
                ... alreadyPresentDexData,
                windowsTotalTradedCountValues: {
                  ... alreadyPresentDexData?.windowsTotalTradedCountValues,
                  [action.payload.window]: data?.totalTradeCounts
                },
                windowsTotalTradedVolumeUSDValues: {
                  ... alreadyPresentDexData?.windowsTotalTradedVolumeUSDValues,
                  [action.payload.window]: data?.totalTradedVolumeUSD
                },
                windowsTotalFeesGeneratedUSD: {
                  ... alreadyPresentDexData?.windowsTotalFeesGeneratedUSD,
                  [action.payload.window]: data?.totalFeesGeneratedUSD
                },
      
                windowsPrices: {
                  ... alreadyPresentDexData?.windowsPrices,
                  [action.payload.window]: data?.prices
                },


                totalTradedVolumeUSD: 
                  lastOrDefault(data?.totalTradedVolumeUSD || [], {unixTimestamp: 0, value: 0}).value,

                totalFeesGeneratedUSD: 
                  lastOrDefault(data?.totalFeesGeneratedUSD || [], {unixTimestamp: 0, value: 0}).value,

                totalTradeCount: 
                  lastOrDefault(data?.totalTradeCounts || [], {unixTimestamp: 0, value: 0}).value

              }]
            }))
          }
        
        });
      },

      setSupplyAndTradeDataForWindow(state, action: SetSupplyAndTradeDataForWindowAction) {
        return postReduce(action.payload.window, {
          ...state,

          windowsTotalTradedTokenVolume: {
            ... state.windowsTotalTradedTokenVolume,
            [action.payload.window]:  action.payload.totalTradeTokenVolumes
          },

          windowsTotalTradedVolumeUSDValues: {
            ... state.windowsTotalTradedVolumeUSDValues,
            [action.payload.window]:  action.payload.totalTradedVolumeUSDValues
          },


          windowsTotalTradeCount: {
            ... state.windowsTotalTradeCount,
            [action.payload.window]: action.payload.totalTradeCountValues
          },

          windowsSupplyValues: {
            ... state.windowsSupplyValues,
            [action.payload.window]: action.payload.supplyValues
          },

        });
      },
    },
  });

  return slice;
}

export type TokenSlice = ReturnType<typeof createTokenSlice>
