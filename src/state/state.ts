import { AppType } from "./slices/app";
import { WindowsBlocksDataType } from "./slices/blocks";
import { DerivativeListType, makeDerivativeSliceName } from "./slices/derivatives";
import { WindowsSynthereumSnapshotData } from "./slices/snapshots";
import { ThemeType } from "./slices/theme";
import { WindowTimestampsType } from "./slices/time";
import { BaseTokenState } from "./slices/token-slice";
import { WindowsTVLValuesData } from "./slices/tvl";
import { WindowsTSVValuesData } from "./slices/tsv";



export interface State {
    theme: ThemeType,
    app: AppType,
    windowsTimestamps: WindowTimestampsType,
    windowsBlocks: WindowsBlocksDataType,
    jrtData: BaseTokenState,
    windowsTVLData: WindowsTVLValuesData,
    windowsTSVData: WindowsTSVValuesData,
    windowsSnapshotsData: WindowsSynthereumSnapshotData,
    derivativeList: DerivativeListType //most recent data for each derivative
    //and one entry per derivative fort historical data (see derivative.ts)
}

export function getAllDerivativeSlicesFromState(state: State): BaseTokenState[]{
    const sliceNames = state.derivativeList.map(makeDerivativeSliceName)
    return (
        Object.entries(state).filter(([key]) => sliceNames.includes(key))
        .map(([_, value]) => value)
    );
}