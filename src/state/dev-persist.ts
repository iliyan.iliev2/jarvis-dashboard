import { Middleware } from '@reduxjs/toolkit';
import { cache } from '../utils/cache';
import { State } from './state';

let lastState: State | null = null;

const IN_DEV_MODE = process.env.NODE_ENV === 'development';

export const createPersistMiddleware = (rootStateKeys: (keyof State)[]) => {

  if(! IN_DEV_MODE){
    return null;
  }


  const persistMiddleware: Middleware = store => next => action => {
    const result = next(action);

    if (cache) {
      const state: State = store.getState();

      if (lastState) {
        rootStateKeys.forEach(rootStateKey => {
          const current = state[rootStateKey as keyof typeof state];
          const previous = lastState![rootStateKey as keyof typeof state];

          if (previous !== current) {
            cache!.set(rootStateKey, current);
          }
        });
      }

      lastState = state;
    }

    return result;
  };

  return {
    middleware: persistMiddleware,
    getPreloadedState(){
      if(cache && rootStateKeys.every(key => cache!.has(key)) && IN_DEV_MODE){
        return Object.fromEntries(
          rootStateKeys.map(key => {
            let data = cache!.get(key)
            console.debug('preloaded', key, data);
            return [key, data] as [string, any]
          })
        );
      }
      return undefined;
    }
  };
};