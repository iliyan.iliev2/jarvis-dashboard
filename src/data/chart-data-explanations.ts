import { TokenDataKind } from "../components/TokenAreaChart/TokenAreaChart";

export const SyntheticDataExplanations: Record<TokenDataKind, string> = {
    //general
    'supply': 'Total supply of the synthetic',
    'total-value': 'Total value of the synthetic : supply x price',

    //price
    'price': 'The real price of the asset (chainlink price). It is retrieved from a subgraph.',

    'dex-price': 
    `The aggregated dex price of a synthetic, it is computed by summing the weighted dex prices. 
     The weight is determined by the volume of the dex (for the synthetic) : if 90% of the volume happens on a dex the weight for the dex will be 0.9`,

    'all-prices': 'Price and aggregated dex price',

    //synthereum volume
    'total-traded-token-volume': 'Volume of traded token on Synthereum since creation (no dex data).',
    'traded-token-volume': 'Volume of traded token on Synthereum (no dex data).',
    'total-traded-usd-volume': 'Volume in USD of traded token on Synthereum since creation (no dex data).',
    'traded-usd-volume': 'Volume in USD of traded token on Synthereum (no dex data).',

    //dex volume
    'dex-total-traded-usd-volume': 'Volume of traded token on major dexs since creation',
    'dex-traded-usd-volume': 'Volume of traded token on major dexs',

    //trade count,
    'synthereum-total-trade-count': 'Number of trades (Mint/Redeem/Exchange) on Synthereum since creation.',
    'dex-total-trade-count': 'Number of swaps on major dexs since creation.',
};