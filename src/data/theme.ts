export const backgroundMap = Object.freeze({
    light: '/images/light-mode-background.jpg',
    dark: '/images/dark-mode-background.jpg',
    night: '/images/night-mode-background.jpg',
});
  