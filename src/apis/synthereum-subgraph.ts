import { debounceAsync, isDefined, PromiseResultType, SecondsTimePeriod } from "../utils";
import { pageResults } from "./the-graph-utils/page-results";

const THE_GRAPH_SYNTHEREUM_URL = 'https://api.thegraph.com/subgraphs/name/aurelienzintzmeyer/synthereum-mainnet';

export async function getDerivatives(){
  
    return pageResults({
        timeout: 10e3,
        api: THE_GRAPH_SYNTHEREUM_URL,
        query: {
            collection: 'derivatives',
            selection: {},
            shallowProps: [
                'id',
                'totalTradedSynthVolume',
                'totalTradedVolumeUSD',
                'totalTradeCount',
                'totalFeePaid',
                'supply',
                'collateralBalance',
                'remainingPoolsCollateral'
            ],
            deepProps: {
                tokenCurrency: {
                    shallowProps: ['id', 'symbol', 'decimals']
                },
                collateralCurrency: {
                    shallowProps: ['symbol'],
                }
            }
        }
    } as const);
}


const SynthereumSnapshotProperties = {
    shallowProps: [
        'id',
        'timestamp',
        'totalValueLockedUSD', 
        'last10MinRelevantSnaphostTimestamp',
        'last1HourRelevantSnaphostTimestamp',
        'last12HoursRelevantSnaphostTimestamp',
    ],
    deepProps: {
        derivativeSnapshots: {
            shallowProps: [
                'id', 
                'supply', 
                'priceUSD',
                'tokenCurrencyAddress', 
                'totalTradeCount',
                'totalTradedSynthVolume',
                'totalTradedVolumeUSD'
            ],
        }
    }
} as const;

export async function getLastSynthereumSnaphost(): Promise<SynthereumSnapshot>{
  
    return pageResults({
        timeout: 10e3,
        api: THE_GRAPH_SYNTHEREUM_URL,
        query: {
            singleEntity: 'synthereum',
            selection: {
                id: 1
            },
            shallowProps: [],
            deepProps: {
                lastSnaphost: SynthereumSnapshotProperties,
            }
        }
    } as const)
    .then(r => r.lastSnaphost);
}

export async function getSynthereumTVL(){
    return Number( (await getLastSynthereumSnaphost()).totalValueLockedUSD );
}



async function _getSynthereumSnapshotsInTimestampRange(range: [number, number]): Promise<SynthereumSnapshot[]> {

    return pageResults({
        timeout: 10e3,
        api: THE_GRAPH_SYNTHEREUM_URL,
        query: {
            collection: 'synthereumSnapshots',
            selection: {
                first: 1,
                where: {
                    timestamp_lte: range[1],
                    timestamp_gte: range[0]
                }
            },
            ...SynthereumSnapshotProperties
        }
    } as const)
}       


async function _getSynthereumSnapshotByTimestamp(timestamp: number) {
    
    return pageResults({
        timeout: 10e3,
        api: THE_GRAPH_SYNTHEREUM_URL,
        query: {
            singleEntity: 'synthereumSnapshot',
            selection: {
                id: timestamp
            },
            ...SynthereumSnapshotProperties
        }
    } as const)
}       

export type SynthereumSnapshot = PromiseResultType<typeof getSynthereumSnapshotByTimestamp>;


async function _getSynthereumSnapshotsByTimestamps(timestamps: number[]): Promise<(SynthereumSnapshot | null)[]> {
        
    return pageResults({
        timeout: 10e3,
        api: THE_GRAPH_SYNTHEREUM_URL,
        queryBlocks: Object.fromEntries(
            timestamps.map(timestamp => {
                let query = {
                    singleEntity: 'synthereumSnapshot',
                    selection: {
                        id: timestamp
                    },
                    ...SynthereumSnapshotProperties
                };
                return ['t' + timestamp.toString(), query]
            })
        )
            
    } as const)
    .then(r => Object.values(r))
}  

async function _getSynthereumSnapshotsAtTimestamps(timestamps: number[], period: SecondsTimePeriod): Promise<SynthereumSnapshot[]> {
    
    const MIN_TIMESTAMP = timestamps[0];
    const SLICE_COUNT = 16;
    const TIMESTAMPS_SLICE_SIZE = Math.floor(timestamps.length / SLICE_COUNT);
    if(TIMESTAMPS_SLICE_SIZE === 0){
        throw new Error(`at least ${SLICE_COUNT} timestamps required`);
    }

    /*
        example for slice count = 4
        [    ] [x   ] [x   ] [x   ]x
        [   x] [   x] [   x] [   x] 
        [  x ] [  x ] [  x ] [  x ] 
        [ x  ] [ x  ] [ x  ] [ x  ] 
        [x   ] [x   ] [x   ] [x   ] 
    */

    let alreadyRetrievedRelevantSnapshotIds = new Set<number>();
    let lastSynthereumSnaphost = await getLastSynthereumSnaphost();
    let snapshots: SynthereumSnapshot[] = new Array(SLICE_COUNT - 1);

    let promises: Promise<any>[] = [];

    for (let i = 1; i < SLICE_COUNT; i++) {
        let choosenTimestampRange: [number, number] = 
            [
                timestamps[i * TIMESTAMPS_SLICE_SIZE], 
                timestamps[(i + 1) * TIMESTAMPS_SLICE_SIZE ]
            ];
        

        let promise = getSynthereumSnapshotsInTimestampRange(choosenTimestampRange);
        promise.then(function(this: {index: number}, foundSnapshotsInRange: SynthereumSnapshot[]){
            if(foundSnapshotsInRange.length !== 0){
                snapshots[this.index] = foundSnapshotsInRange[0];
            }
        }.bind({index : i - 1}));

        promises.push(promise);
    }

    await Promise.all(promises);
    promises.length = 0;

    snapshots.push(lastSynthereumSnaphost);
    snapshots = snapshots.filter(isDefined);
    snapshots.forEach(snapshot => alreadyRetrievedRelevantSnapshotIds.add(Number(snapshot.id)));
    
    let i = 0;
    while(i !== SLICE_COUNT){
        i++;
    }

    let currentSnaphosts: SynthereumSnapshot[] = [...snapshots];
    let lastRelevantSnaphostTimestamp_Key: keyof SynthereumSnapshot;
    let lastRelevantSnaphostTimestamp_LargerLevelKey: keyof SynthereumSnapshot | undefined;

    switch(period){
        case SecondsTimePeriod.TenMinutes: 
            lastRelevantSnaphostTimestamp_Key = 'last10MinRelevantSnaphostTimestamp';
            lastRelevantSnaphostTimestamp_LargerLevelKey = 'last1HourRelevantSnaphostTimestamp';
            break;
        case SecondsTimePeriod.OneHour: 
            lastRelevantSnaphostTimestamp_Key = 'last1HourRelevantSnaphostTimestamp';
            lastRelevantSnaphostTimestamp_LargerLevelKey = 'last12HoursRelevantSnaphostTimestamp';
            break;
        case SecondsTimePeriod.TwelveHours: 
            lastRelevantSnaphostTimestamp_Key = 'last12HoursRelevantSnaphostTimestamp'; 
            break;
        default:
            throw new Error('not supported time period :' + period);
    }



    function removeUselessSnapshots(snapshots: SynthereumSnapshot[]){
        return snapshots
            .filter(({id}) => Number(id) >= MIN_TIMESTAMP)
            .filter(snapshot => ! alreadyRetrievedRelevantSnapshotIds.has(Number(snapshot[lastRelevantSnaphostTimestamp_Key])));
    }

    do {
        currentSnaphosts = removeUselessSnapshots(currentSnaphosts);

        let snapshotIdsToRetrieve = 
            currentSnaphosts.map(currSnap => Number(currSnap[lastRelevantSnaphostTimestamp_Key]))

        currentSnaphosts = (await getSynthereumSnapshotsByTimestamps(snapshotIdsToRetrieve)).filter(isDefined);
        currentSnaphosts.forEach(snapshot => alreadyRetrievedRelevantSnapshotIds.add(Number(snapshot.id)));

        currentSnaphosts = removeUselessSnapshots(currentSnaphosts);
        snapshots.push(...currentSnaphosts);

        if(currentSnaphosts.length !== 0 && currentSnaphosts.length <= 2 && lastRelevantSnaphostTimestamp_LargerLevelKey){
            for await(let snapshot of [... currentSnaphosts]){
                let additionalSnapshotId = Number(snapshot[lastRelevantSnaphostTimestamp_LargerLevelKey]);
                currentSnaphosts.push( await getSynthereumSnapshotByTimestamp(additionalSnapshotId) );
            }

            currentSnaphosts.forEach(snapshot => alreadyRetrievedRelevantSnapshotIds.add(Number(snapshot.id)));
        }
    } while( currentSnaphosts.length !== 0 );


    snapshots = snapshots.map(snapshot => ({...snapshot, timestamp: Number(snapshot.timestamp)}));
    snapshots.sort((a, b) => a.timestamp as number - (b.timestamp as number));
    return snapshots;
}



const getSynthereumSnapshotsInTimestampRange = debounceAsync(_getSynthereumSnapshotsInTimestampRange);
const getSynthereumSnapshotByTimestamp = debounceAsync(_getSynthereumSnapshotByTimestamp);
export const getSynthereumSnapshotsByTimestamps = debounceAsync(_getSynthereumSnapshotsByTimestamps);
export const getSynthereumSnapshotsAtTimestamps = debounceAsync(_getSynthereumSnapshotsAtTimestamps);