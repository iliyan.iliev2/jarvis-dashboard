import { SecondsTimePeriod, splitArrayIntoSubArrays, splitAsyncOperation, TimestampedValue } from "../utils";
import { AbiItem, Contract, makeBatchedCallsAtBlocks, web3 } from "../utils/web3";
import { ABI as AggregatorABI } from "./data/chainlink-aggregator-abi";
import { pageResults, SingleQueryDescription } from "./the-graph-utils/page-results";



const CHAINLINK_PRICE_DECIMALS = 8;

const SYNTH_SYMBOL_TO_CHAINLINK_AGGREGATOR_PROXY_ADDRESS = new Map<string, string>([
    ['EUR', '0xb49f677943bc038e9857d61e7d053caa2c1734c1'],
    ['CHF', '0x449d117117838ffa61263b61da6301aa2a88b13a'],
    ['GBP', '0x5c0ab2d9b5a7ed9f470386e82bb36a3613cdd4b5'],
    ['XAU', '0x214ed9da11d2fbe465a6fc601a91e62ebec1a0d6']
]);

const SYNTH_SYMBOL_TO_CHAINLINK_AGGREGATOR_PROXY_CONTRACT = new Map<string, Contract>();

function cleanSymbol(symbol: string){
    return symbol.replace(/^j/, '');
}

//NOT CURRENTLY USED

function createChainlinkContracts(){
    for(let [symbol, address] of Array.from(SYNTH_SYMBOL_TO_CHAINLINK_AGGREGATOR_PROXY_ADDRESS.entries())){

        SYNTH_SYMBOL_TO_CHAINLINK_AGGREGATOR_PROXY_CONTRACT.set(
            symbol, 
            new web3.eth.Contract(AggregatorABI as AbiItem[], address)
        );
    }
}

export async function getPriceOf(symbol: string, block?: number): Promise<number> {
    symbol = cleanSymbol(symbol);

    let contract = SYNTH_SYMBOL_TO_CHAINLINK_AGGREGATOR_PROXY_CONTRACT.get(symbol);
    if(contract === undefined) throw new Error('invalid symbol ' + symbol);
    return Number(
        await contract.methods.latestAnswer().call(undefined, block)
    ) / (10 ** CHAINLINK_PRICE_DECIMALS);
}


export async function getPricesAtBlocks(symbol: string, blocks: number[]): Promise<number[]> {
    symbol = cleanSymbol(symbol);

    if(blocks.length > 39){
        const subarrays = splitArrayIntoSubArrays(blocks, 30);
        let prices: number[] = [];
        for await(let array of subarrays){
            prices.push(... await getPricesAtBlocks(symbol, array));
        }
        return prices.flat();
    }
    return (await Promise.all(blocks.map(block => getPriceOf(symbol, block)))).flat();
}


export async function getBatchedPricesAtBlocks(symbol: string, blocks: number[]): Promise<number[]> {
    symbol = cleanSymbol(symbol);

    let contract = SYNTH_SYMBOL_TO_CHAINLINK_AGGREGATOR_PROXY_CONTRACT.get(symbol);
    if(contract === undefined) throw new Error('invalid symbol ' + symbol);

    let results = await makeBatchedCallsAtBlocks(contract, {
        blocks,
        signature: 'latestAnswer()'
    });

    return results.map(
        result => Number( web3.eth.abi.decodeParameter('uint256', result))
                  / (10 ** CHAINLINK_PRICE_DECIMALS)
    );

}

//NOT CURRENTLY USED ------ END


//
// !! COMMUNITY SUBGRAPH !!
//

const GRAPH_URL = 'https://api.thegraph.com/subgraphs/name/openpredict/chainlink-prices-subgraph'


const PriceProps = {
    shallowProps: [
        'id', 
        'price', 
        'timestamp'
    ],
} as const;

function makeAssetUSDPair(asset: string){
    asset = cleanSymbol(asset);
    return `${asset}/USD`;
}

export async function community_subgraph_getLastPriceForAsset(asset: string){

    const assetPair = makeAssetUSDPair(asset);

    return pageResults({
        timeout: 10e3,
        api: GRAPH_URL,
        max: 50,
        query: {
            collection: 'prices',
            selection: {
                where: {
                    assetPair,
                },
                orderBy: 'timestamp',
                orderDirection: 'desc',
            },
            ...PriceProps,
        }
    }).then(r => Number(r[0].price) / (10 ** CHAINLINK_PRICE_DECIMALS));

}

export async function community_subgraph_getPricesForAssetAtTimestamps(asset: string, timestamps: number[], recursiveSampling = false): Promise<TimestampedValue[]>{


    const interval = timestamps[1] - timestamps[0];
    const assetPair = makeAssetUSDPair(asset);


    if(interval >= SecondsTimePeriod.TwelveHours || recursiveSampling){

        const BLK_COUNT_IN_SINGLE_REQ = 8;

        let samplingTimestamps = recursiveSampling ? timestamps : timestamps.map(timestamp => [timestamp, timestamp + interval / 2]).flat();

        if(samplingTimestamps.length > BLK_COUNT_IN_SINGLE_REQ){
            let results = await splitAsyncOperation(
                BLK_COUNT_IN_SINGLE_REQ, 
                samplingTimestamps, timestamps => community_subgraph_getPricesForAssetAtTimestamps(asset, timestamps, true)
            );

            return results.reduce((array, e, i) => {
                let prev = array[array.length - 1];
                if(e.value === 0 && prev !== undefined) {
                    array.push({unixTimestamp: e.unixTimestamp, value: prev.value});
                } else array.push(e);
                return array;
            }, [] as TimestampedValue[]);
        }
    

        return await pageResults({
            timeout: 20e3,
            api: GRAPH_URL,
            queryBlocks: Object.fromEntries(
                samplingTimestamps.map((timestamp, i) => {
                    let query: SingleQueryDescription = {
                        collection: 'prices',
                        selection: {
                            where: {
                                assetPair,
                                timestamp_gte: String(timestamp),
                                timestamp_lte: String(timestamp + 20 * SecondsTimePeriod.TwelveHours)
                            },
                            first: 1,
                        },
                        ...PriceProps
                    };
                    return ['t' + timestamp.toString(), query]
                })
            )
        }).then(r => {
                    
            let sampledPrices: (number|null)[] = [];
            const entries: [string, any][] = Object.entries(r);

            for (let [row_id, row] of entries) {
                if(String(row_id).startsWith('t')){
                    let price = row[0]?.price ?? 0;
                    
                    sampledPrices.push(
                        Number(price) / (10 ** CHAINLINK_PRICE_DECIMALS)
                    );
                }
            }


            let prices: TimestampedValue[] = [];
            for(let i = 0; i < samplingTimestamps.length - 1; i += 2){
                //defaults to other value if zero
                let price1 = sampledPrices[i];
                let price2 = sampledPrices[i + 1] || price1;
                price1 = price1 || price2;

                prices.push({
                    unixTimestamp: samplingTimestamps[i],
                    value: (Number(price1) + Number(price2))/2
                });
            }



            return prices;
        })
    }


    return pageResults({
        timeout: 10e3,
        api: GRAPH_URL,
        max: 100_000,
        query: {
            collection: 'prices',
            selection: {
                where: {
                    assetPair,
                    timestamp_gte: String(timestamps[0]),
                    timestamp_lte: String(timestamps[timestamps.length - 1]),
                },
                orderBy: 'timestamp',
                orderDirection: 'asc',
            },
            ...PriceProps,
        }
    })
    .then(r => {

        let rawResults = Array.from(r).map(e => ({
            price: Number(e.price) / (10 ** CHAINLINK_PRICE_DECIMALS), 
            timestamp: Number(e.timestamp)
        }));

        function findClosestPrice(timestamp: number){
            return rawResults.reduce((_closestPrice, price) => {
                if(
                    Math.abs(price.timestamp - timestamp) < Math.abs(_closestPrice.timestamp - timestamp)
                ){
                    _closestPrice = price;
                }
                return _closestPrice;
            })
        }

        return timestamps.map(timestamp => ({
            unixTimestamp: timestamp,
            value: findClosestPrice(timestamp).price
        }));
    });
}