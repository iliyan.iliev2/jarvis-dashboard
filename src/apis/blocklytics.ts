import { splitAsyncOperation } from "../utils";
import { Block } from "./common";
import { pageResults, SingleQueryDescription } from "./the-graph-utils/page-results";

const THE_GRAPH_BLOCKLYTICS_URL = "https://api.thegraph.com/subgraphs/name/blocklytics/ethereum-blocks"

export async function getBlockNumbersByTimestamps(timestamps: number[]): Promise<Block[]> {
  
    const SINGLE_REQ_ENTIY_COUNT = 10;

    if(timestamps.length === 0) return [];

    if(timestamps.length > SINGLE_REQ_ENTIY_COUNT){
        return splitAsyncOperation(SINGLE_REQ_ENTIY_COUNT, timestamps, tstamps => getBlockNumbersByTimestamps(tstamps))
    }

    return pageResults({
        timeout: 10e3,
        api: THE_GRAPH_BLOCKLYTICS_URL,
        queryBlocks: Object.fromEntries(
            timestamps.map((t): [string, SingleQueryDescription] => {
                return ['timestamp' + t, {
                    collection: 'blocks',
                    selection: {
                        first: 1,
                        orderBy: 'timestamp',
                        orderDirection: 'desc',
                        where: {
                            timestamp_gt: t,
                            timestamp_lt: t + 600 //block time
                        }
                    },
                    shallowProps: ['number'],
                }]
            })
        )
    })
    .then(r => {
        let blocks: Block[] = [];
        if (r) {
            for (let timestamp in r) {
                if (r[timestamp].length > 0) {
                    blocks.push({
                        timestamp: (timestamp as any).split('timestamp')[1] as number,
                        number: parseInt(r[timestamp][0]['number']),
                    })
                }
            }
        }
        return blocks;
    })
}
