import { PromiseResultType, SecondsTimePeriod, splitAsyncOperation, takeOneElemIn, TimestampedValue, TimeWindow } from "../utils";
import { Block } from "./common";
import { pageResults, SingleQueryDescription } from "./the-graph-utils/page-results";

const THE_GRAPH_BALANCER_URL = "https://api.thegraph.com/subgraphs/name/balancer-labs/balancer"

const DEFAULT_TIMEOUT = 15000;

export async function getPoolTokens(tokenSymbol: string) {
  
    return pageResults({
        timeout: DEFAULT_TIMEOUT,
        api: THE_GRAPH_BALANCER_URL,
        query: {
            collection: 'poolTokens',
            selection: {
                where: {
                    symbol: tokenSymbol
                }
            },
            shallowProps: [
                'id',
                'symbol',
                'name',
                'balance'
            ],
            deepProps: {
                poolId: {
                    shallowProps: ['id', 'symbol', 'name']
                }
            }
        }
    });
}

const PoolProperties = {
    shallowProps: [
        'id',
        'symbol',
        'swapsCount',
        'swapFee',
        'totalSwapVolume',
    ],
} as const;

export async function getPoolInfo(poolId: string) {
    return pageResults({
        timeout: DEFAULT_TIMEOUT,
        api: THE_GRAPH_BALANCER_URL,
        query: {
            singleEntity: 'pool',
            selection: {
                id: poolId
            },
            ... PoolProperties
        }
    });
}

export async function getAllPoolsForToken(tokenSymbol: string){
    let poolTokens = await getPoolTokens(tokenSymbol);
    return Promise.all(
        poolTokens.map(e => getPoolInfo(String(e.poolId.id)))
    );
}
export async function getTokenPrice(tokenAddress: string) {
    return pageResults({
        timeout: DEFAULT_TIMEOUT,
        api: THE_GRAPH_BALANCER_URL,
        query: {
            singleEntity: 'tokenPrice',
            selection: {
                id: tokenAddress,
            },
            shallowProps: [
                'price'
            ]
        }
    });
}

export type BalancerPoolInfo = PromiseResultType<typeof getPoolInfo>;
export type TimestampedBalancerPoolInfo = Record<keyof BalancerPoolInfo, number> & {
    unixTimestamp: number
}

export async function getPoolDataAtBlocks({poolId, blocks}: {poolId: string, blocks: Block[]}): Promise<TimestampedBalancerPoolInfo[]> {
    const BLK_COUNT_IN_SINGLE_REQ = 8;
  
    if(blocks.length > BLK_COUNT_IN_SINGLE_REQ){
        return splitAsyncOperation(
            BLK_COUNT_IN_SINGLE_REQ, 
            blocks, 
            blks => getPoolDataAtBlocks({poolId, blocks: blks})
        );
    }

    return pageResults({
        timeout: DEFAULT_TIMEOUT,
        api: THE_GRAPH_BALANCER_URL,
        queryBlocks: Object.fromEntries([
            ... blocks.map((block): [string, SingleQueryDescription] => {
                return ['timestamp' + block.timestamp, {
                    singleEntity: 'pool',
                    selection: {
                        id: poolId,
                        block: { number: block.number },
                    },
                    ... PoolProperties
                }]
            })
        ])
    }).then(r => {
        /*result shape : {
             timestampXXXX: { ... pool info ... } , 
             timestampXXXX: { ... pool info ... } , 
             ...
           }
        */

        const entries: [string, any][] = Object.entries(r);
        const poolInfoList: TimestampedBalancerPoolInfo[] = [];

       for (let [row_id, row] of entries) {
            if(String(row_id).includes('timestamp')){
                let timestamp = Number( String(row_id).split('timestamp')[1] );
                poolInfoList.push(row ? {
                    id: row.id,
                    symbol: row.symbol,
                    unixTimestamp: timestamp, 
                    swapFee: parseFloat(row.swapFee),
                    swapsCount: parseInt(row.swapsCount),
                    totalSwapVolume: parseFloat(row.totalSwapVolume)
                } : {
                    id: '',
                    unixTimestamp: timestamp, 
                    symbol: '',
                    swapFee: 0,
                    swapsCount: 0,
                    totalSwapVolume: 0
                });
            }
        }

        return poolInfoList;
    });
}



export async function getBalancerTokenPriceAtBlocks
({tokenAddress, blocks, timeWindow}: {tokenAddress: string, blocks: Block[], timeWindow: TimeWindow}): Promise<TimestampedValue[]> {
    const BLK_COUNT_IN_SINGLE_REQ = 8;
  
    let reduceSampling = timeWindow >= TimeWindow.MONTH;


    if(blocks.length > BLK_COUNT_IN_SINGLE_REQ){
        return splitAsyncOperation(
            BLK_COUNT_IN_SINGLE_REQ, 
            blocks, 
            blks => getBalancerTokenPriceAtBlocks({tokenAddress, blocks: blks, timeWindow})
        );
    }

    if(reduceSampling){
        blocks = takeOneElemIn(blocks, 2);
    }


    return pageResults({
        timeout: DEFAULT_TIMEOUT,
        api: THE_GRAPH_BALANCER_URL,
        queryBlocks: Object.fromEntries([
            ... blocks.map((block): [string, SingleQueryDescription] => {
                return ['timestamp' + block.timestamp, {
                    singleEntity: 'tokenPrice',
                    selection: {
                        id: tokenAddress,
                        block: { number: block.number }
                    },
                    shallowProps: [
                        'price'
                    ]
                }]
            })
        ])
    }).then(r => {
        /*result shape : {
             timestampXXXX: { price: <value> } , 
             timestampXXXX: { price: <value> } , 
             ...
           }
        */

        const entries: [string, any][] = Object.entries(r);
        const prices: TimestampedValue[] = [];

       for (let [row_id, row] of entries) {
            if(String(row_id).includes('timestamp')){
                let timestamp = Number( String(row_id).split('timestamp')[1] );
                prices.push(row ? {
                    unixTimestamp: timestamp, 
                    value: parseFloat(row.price)
                } : {
                    unixTimestamp: timestamp, 
                    value: 0
                });

                if(reduceSampling){
                    prices.push(prices[prices.length - 1]);
                }
            }
        }

        return prices;
    });
}

