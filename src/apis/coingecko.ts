import { makeUTimestampRange } from '../utils';
import { timedOutFetch } from '../utils/fetch';

const API = "https://api.coingecko.com/api/v3"
const JRT_TOKEN_ID = "jarvis-reward-token";
const TIMEOUT = {timeout: 3000};




export function getETH_USD_Price(): Promise<number> {
    return timedOutFetch({api: API + "/simple/price?ids=ethereum&vs_currencies=USD", ... TIMEOUT})
    .then(r => r.json())
    .then(r => r.ethereum.usd)
}

export function getCoinMarketInfoFromRange(coin_id: string, range: [number, number]){
    return timedOutFetch({
        api: API + `/coins/${coin_id}/market_chart/range?vs_currency=usd&from=${range[0]}&to=${range[1]}`,
        ... TIMEOUT
    })
    .then(r => r.json());
}

export function getCurrentCoinMarketInfo(coin_id: string){
    return getCoinMarketInfoFromRange(coin_id, makeUTimestampRange({from_seconds_ago: 1000}))
}

export function getCurrentCoinMarketCap(coin_id: string){
    return getCurrentCoinMarketInfo(coin_id)
    .then(r => Number(r.market_caps[r.market_caps.length - 1][1]));
}

export function getCurrentJRTMarketCap(){
    return getCurrentCoinMarketCap(JRT_TOKEN_ID);
}