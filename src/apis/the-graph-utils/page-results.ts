//inspired from
//https://github.com/justinjmoses/graph-results-pager/blob/master/index.js
//-------------------------------------------------------------------------
//  !!!!
//  uses AbortController for timeout (no timeout if not supported) : https://caniuse.com/?search=abortcontroller
//  !!!!
import { RecReadonly } from "../../utils";
import { timedOutFetch } from "../../utils/fetch";


const MAX_PAGE_SIZE = 1000; // The Graph max page size

type Deep = RecReadonly<{
	shallowProps: Readonly<string[]>, 
	deepProps?: {
		[key in string]?: Deep
	}
}>;

export type PropertiesDescription = {
	shallowProps: string[],
	deepProps?: Deep
}

export type SingleQueryDescription = ({                
	collection: string,     
	selection: {
		where?: Object,
		first?: number,
		skip?: number,
		orderBy?: string,
		orderDirection?: 'asc' | 'desc'
	}
} | {
	singleEntity: string,     
	selection: { [key in string]?: number | string | {[key in string] : number | string } }, 
}) & Deep

export type BlockQueryDescription = {
	[key in string]: SingleQueryDescription
}

//timeout: Number of ms timeout for any single graph paging result (default: 10seconds)
//Maximum number of results to return (default: Infinity)
type QueryDescription = {
	api: string,      
	timeout?: number,       
	max?: number
	query?: SingleQueryDescription
	queryBlocks?: BlockQueryDescription
}


type ArrifyIfTrue<Bool, T> = Bool extends true ? T[] : T

type DeepToResult<Props extends Deep> = 
	(
		Record<Props['shallowProps'][number], string | number | boolean | null>
	)
	& (
		Props['deepProps'] extends undefined ? {}
		: {
			[key in keyof Props['deepProps']]:  
				Props['deepProps'][key] extends {shallowProps: any} ?
					ArrifyIfTrue<
						key extends `${infer U}s` ? true : false,
						DeepToResult<Props['deepProps'][key]>
					>
				: {}
		}
	)

type SingleQueryResult<Query extends SingleQueryDescription> = 
	Query extends {collection: any} ? DeepToResult<Query>[]
	: Query extends {singleEntity: any} ? DeepToResult<Query>
	: any

type BlockQueryResult<Query extends RecReadonly<BlockQueryDescription>> = any

type QueryResult<Query extends QueryDescription> = 
	Query['query'] extends SingleQueryDescription ? SingleQueryResult<Query['query']>
	: Query['queryBlocks'] extends SingleQueryDescription ? BlockQueryResult<Query['queryBlocks']>
	: any


const selectionToString = (obj: any, {removeTopDelimiters = 1}): any =>{
	if(typeof obj === 'string') return "\\\"" + String(obj) + "\\\"";
	if(typeof obj !== 'object') return obj;
	if(Array.isArray(obj)){
		return (removeTopDelimiters ? '' : '[') + 
			obj.filter(value => typeof value !== 'undefined')
			.map(e => selectionToString(e, {removeTopDelimiters: removeTopDelimiters - 1})).join(',') 
		+ (removeTopDelimiters >= 1 ? '' : ']');
	}

	return (removeTopDelimiters >= 1 ? '' : '{') +
		Object.entries(obj)
			.filter(([, value]: any) => typeof value !== 'undefined')
			.map(([key, value]: any) => `${key}:${selectionToString(value, {removeTopDelimiters: removeTopDelimiters - 1})}`)
			.join(',')
		+ (removeTopDelimiters >= 1 ? '' : '}');
}


const propertiesToString = (arg: any): any =>{
	if(typeof arg !== 'object') return arg;
	if(Array.isArray(arg)){
		return  arg.filter(value => typeof value !== 'undefined')
			.map(e => propertiesToString(e)).join(',') ;
	}
	if(typeof arg === 'object' && (arg.deepProps || arg.shallowProps)){
		let sep = 
			(Object.keys(arg.shallowProps || {}).length !== 0 && Object.keys(arg.deepProps || {}).length !== 0) ? ' , ' : ' ';

		return propertiesToString(arg.shallowProps)
		+ sep + ((arg.deepProps && propertiesToString(arg.deepProps as any) || ''));
	}
	return Object.entries(arg)
			.filter(([, value]: any) => typeof value !== 'undefined')
			.map(([key, value]: any) => `${key} { ${propertiesToString(value)} }`)
			.join(',');
}

const build_subquery = (queryDesc: SingleQueryDescription, first?: number, skip?: number) => {

	if('collection' in queryDesc){
		const {collection, selection, shallowProps = [], deepProps = {shallowProps: []}} = queryDesc;
		selection.first = selection.first ?? first;
		selection.skip = selection.skip ?? skip;


		let shallowPropsString = shallowProps.join(', ');
		let deepPropsString = propertiesToString(deepProps);
		let sep = (shallowPropsString !== '' && deepPropsString !== '') ? ',' : '';


		return `${collection}(${selectionToString(selection, {})}){ ${shallowProps} ${sep} ${deepPropsString} }`;
	} else {
		const {singleEntity, selection, shallowProps = [], deepProps = {shallowProps: []}} = queryDesc;
	
		let shallowPropsString = shallowProps.join(', ');
		let deepPropsString = propertiesToString(deepProps);
		let sep = (shallowPropsString !== '' && deepPropsString !== '') ? ',' : '';

		return `${singleEntity}(${selectionToString(selection, {})}){ ${shallowProps} ${sep} ${deepPropsString} }`;
	}
}

const build_query_block = (desc: BlockQueryDescription) => {
	return Object.entries(desc).map(([key, subquery]) => {
		return `${key} : ${build_subquery(subquery)}`
	})
}



export const pageResults = <QDesc extends QueryDescription>({ api, query, queryBlocks, timeout = 10e3, max = Infinity }: QDesc): Promise<QueryResult<QDesc>> => {
	max = Number(max);
	const pageSize = MAX_PAGE_SIZE;

	if((query === undefined) === (queryBlocks === undefined)){
		throw new Error(`Only query or queryBlocks must be provided,  query = ${query}, queryBlock = ${queryBlocks}`);
	}

	if(queryBlocks !== undefined && Object.values(queryBlocks).length === 0){
		return Promise.resolve({} as QueryResult<QDesc>);
	}

	// Note: this approach will call each page in linear order, ensuring it stops as soon as all results
	// are fetched. This could be sped up with a number of requests done in parallel, stopping as soon as any return
	// empty. - JJM
	const runner = ({ firstRun, skip }: {firstRun: boolean, skip: number}): any => {
		skip = (firstRun && query && ('collection' in query) && query?.selection.skip) || skip ;

		const first = skip + pageSize > max ? max % pageSize : pageSize;

		const body: string = 
			query ? 
				`{"query":"{ ${build_subquery(query, first, skip)}}", "variables": null}`
			: queryBlocks ?
			 	`{"query":" query blocks { ${build_query_block(queryBlocks)} }", 
					"variables": null, 
					"operationName": "blocks"
				 }`
			: '';

		// mix the page size and skip fields into the selection object

		// support query logging in nodejs
		if (typeof process === 'object' && typeof process.env === 'object' && process.env.DEBUG === 'true') {
			console.log(body);
		}

			return timedOutFetch({
				api,
				method: 'POST',
				body,
				timeout
			})
			.then(response => {
                return response.json();
            })
            .then(json => {
				if (json.errors) {
					throw Error(JSON.stringify(json.errors));
				}

	
				if(query){

					if('collection' in query){
						const {
							data: { [query.collection]: results },
						} = json;
		
						if (results.length < pageSize || Math.min(max, skip + results.length) >= max) {
							return results;
						}
	
						return runner({ firstRun: false, skip: skip + pageSize }).then((newResults: any) => results.concat(newResults));
					}

					return json.data[query.singleEntity];
				} 
				//queryBlocks

				const key_length = Object.keys(json.data).length;
				if (key_length < pageSize || Math.min(max, skip + key_length) >= max) {
					return json.data;
				}

				return runner({ firstRun: false, skip: skip + pageSize }).then(
					(newResults: any) => ({...json.data, ...newResults}) 
				);
			});
	};

	return runner({ firstRun: true, skip: 0 });
};

