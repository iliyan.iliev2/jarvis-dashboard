export type Block = {
    timestamp: number,
    number: number
}