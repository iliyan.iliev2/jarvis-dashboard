import { splitAsyncOperation } from "../utils";
import { cleanAddressAndRemoveChecksum } from "../utils/web3";
import { Block } from "./common";
import { USDC_ADDRESS, WETH_ADDRESS } from "./data/contract-addresses";
import { pageResults, SingleQueryDescription } from "./the-graph-utils/page-results";

//the Sushiswap subgraph has the same GraphQL API as the UniswapV2 subgraph
const THE_GRAPH_UNISWAP_URL = 'https://api.thegraph.com/subgraphs/name/uniswap/uniswap-v2';
const THE_GRAPH_SUSHISWAP_URL = 'https://api.thegraph.com/subgraphs/name/croco-finance/sushiswap';


const UNISWAP_FEE = 0.03; 
const SUSHISWAP_FEE = 0.025;  //0.025 (0.25%) for LP, 0.005 (0.05%) for Sushi holders


function _getPairInfoByTokenAddresses(token0Address: string, token1Address: string, sushiswap: boolean){

    const URL = sushiswap ? THE_GRAPH_SUSHISWAP_URL : THE_GRAPH_UNISWAP_URL;

    return pageResults({
        timeout: 10e3,
        api: URL,
        query: {
            collection: 'pairs',
            selection: {
                where: {
                    token0: token0Address,
                    token1: token1Address
                }
            },
            shallowProps: [
                'id',
                'token0Price',
                'token1Price',
            ]
        }
    });
}

export async function getPairInfoByTokenAddresses(token0Address: string, token1Address: string, sushiswap: boolean){
    token0Address = cleanAddressAndRemoveChecksum(token0Address);
    token1Address = cleanAddressAndRemoveChecksum(token1Address);

    const error = "Pair does not exist";
    const right_dir_pair_result = await _getPairInfoByTokenAddresses(token0Address, token1Address, sushiswap);

    if(right_dir_pair_result.length === 1){
        return right_dir_pair_result[0];
    }
    const opposite_dir_pair_result = await _getPairInfoByTokenAddresses(token1Address, token0Address, sushiswap);
    if(opposite_dir_pair_result.length === 0){
        throw error;
    }
    
    //swap prices
    const pair_result = opposite_dir_pair_result[0];
    
    const token0Price = pair_result.token0Price;
    pair_result.token0Price = pair_result.token1Price ;
    pair_result.token1Price = token0Price;

    return pair_result;
}

/**
 * 
 * @param token0Address 
 * @param token1Adddress
 * @returns token1 per token0 
 */
export async function getPairPriceByTokenAddresses(token0Address: string, token1Adddress: string, sushiswap: boolean){
    return getPairInfoByTokenAddresses(cleanAddressAndRemoveChecksum(token0Address), cleanAddressAndRemoveChecksum(token1Adddress), sushiswap)
    .then(r => r?.token1Price);
}

export async function getPairId(token0Address: string, token1Adddress: string, sushiswap: boolean) {
    return getPairInfoByTokenAddresses(cleanAddressAndRemoveChecksum(token0Address), cleanAddressAndRemoveChecksum(token1Adddress), sushiswap)
    .then(r => r?.id);
}

export async function getTokenETHPairPrice(tokenAddress: string, sushiswap: boolean) {
    return getPairPriceByTokenAddresses(tokenAddress, WETH_ADDRESS, sushiswap);
}

export async function getETH_USDCPrice() {
    return getPairPriceByTokenAddresses(WETH_ADDRESS, USDC_ADDRESS, false);
}

export async function getTokenDerivedEth({tokenAddress, sushiswap}: {tokenAddress: string, sushiswap: boolean}){
    tokenAddress = cleanAddressAndRemoveChecksum(tokenAddress);

    const URL = sushiswap ? THE_GRAPH_SUSHISWAP_URL : THE_GRAPH_UNISWAP_URL;

    return pageResults({
        timeout: 10e3,
        api: URL,
        query: {
            singleEntity: 'token',
            selection: {
                id: tokenAddress
            },
            shallowProps: [
                'id',
                'derivedETH',
            ],
        }
    })
    .then((r: any) => r?.derivedETH);
}


export async function getTokenUSD_PriceHistory({tokenAddress, min_timestamp = 0, sushiswap = false}: {tokenAddress: string, min_timestamp?: number, sushiswap: boolean}){
    tokenAddress = cleanAddressAndRemoveChecksum(tokenAddress);

    const URL = sushiswap ? THE_GRAPH_SUSHISWAP_URL : THE_GRAPH_UNISWAP_URL;

    return await pageResults({
        timeout: 10e3,
        api: URL,
        query: {
            collection: 'tokenDayDatas',
            selection: {
                where: { 
                    token: tokenAddress,
                    date_gte: min_timestamp,
                },
                orderBy: 'date',
                orderDirection: 'desc'
            },
            shallowProps: [
                'date',
                'priceUSD',
            ], 
        }
    });
}

export type TimestampedDexTokenInfo = {
    unixTimestamp: number,
    totalVolumeUSD: number,
    totalFeesGeneratedUSD: number,
    txCount: number,
    totalLiquidity: number,
    priceUSD: number
}

export async function getTokenInfoByBlocks({tokenAddress, blocks, sushiswap = false}: {tokenAddress: string, blocks: Block[], sushiswap?: boolean}): Promise<TimestampedDexTokenInfo[]>{
    const BLK_COUNT_IN_SINGLE_REQ = 8;
  
    tokenAddress = cleanAddressAndRemoveChecksum(tokenAddress);
    const FEE = sushiswap ? SUSHISWAP_FEE : UNISWAP_FEE;
    const URL = sushiswap ? THE_GRAPH_SUSHISWAP_URL:  THE_GRAPH_UNISWAP_URL;

    if(blocks.length > BLK_COUNT_IN_SINGLE_REQ){
        return splitAsyncOperation(BLK_COUNT_IN_SINGLE_REQ, blocks, blks => getTokenInfoByBlocks({tokenAddress, blocks: blks, sushiswap}));
    }

    return pageResults({
        timeout: 10e3,
        api: URL,
        queryBlocks: Object.fromEntries([
            ... blocks.map((block): [string, SingleQueryDescription] => {
                return ['timestamp' + block.timestamp, {
                    singleEntity: 'token',
                    selection: {
                        id: tokenAddress,
                        block: { number: block.number },
                    },
                    shallowProps: [
                        'derivedETH',
                        'txCount',
                        'tradeVolume',
                        'tradeVolumeUSD',
                        'untrackedVolumeUSD',
                        'totalLiquidity',
                        'totalSupply',
                    ],
                }]
            }),
            //bundle: https://uniswap.org/docs/v2/API/entities/#bundle
            ... blocks.map((block): [string, SingleQueryDescription] => {
                return ['bundle' + block.timestamp, {
                    singleEntity: 'bundle',
                    selection: {
                        id: "1",
                        block: { number: block.number },
                    },
                    shallowProps: ['ethPrice'],
                }]
            })
        ])
    })
    .then(r => {
        /*result shape : {
             timestampXXXX: { derivedEth: <value>, ... } , 
             timestampXXXX: { derivedEth: <value>, ... } , 
             ...
             bundleXXXX: { ethPrice: <value>  }
             bundleXXXX: { ethPrice: <value>  }
             ...
           }
        */

        type RawTokenInfo = {
            unixTimestamp: number, 
            derivedETH: number,
            untrackedVolumeUSD: number,
            txCount: number,
            totalLiquidity: number,
        }

    
        const rawTokenInfo: RawTokenInfo[] = [];
        const tokenInfo: TimestampedDexTokenInfo[] = [];
        
        const entries: [string, any][] = Object.entries(r);

        for (let [row_id, row] of entries) {
            if(String(row_id).includes('timestamp')){
                let timestamp = Number( String(row_id).split('timestamp')[1] );
                rawTokenInfo.push(row ? {
                    unixTimestamp: timestamp, 
                    derivedETH: parseFloat(row.derivedETH),
                    untrackedVolumeUSD: parseFloat(row.untrackedVolumeUSD),
                    txCount: Number(row.txCount),
                    totalLiquidity: parseFloat(row.totalLiquidity)
                } : {
                    unixTimestamp: timestamp, 
                    derivedETH: 0,
                    untrackedVolumeUSD: 0,
                    txCount: 0,
                    totalLiquidity: 0
                });
            }
        }

        let i = 0;
        for (let [bundle_name, bundle] of entries) {
            let _bundle_name = String(bundle_name);

            if(_bundle_name.includes('bundle')){
                let timestamp = Number( _bundle_name.split('bundle')[1] );
                if (timestamp) {
                    tokenInfo.push({
                        unixTimestamp: timestamp,
                        priceUSD: bundle.ethPrice * rawTokenInfo[i].derivedETH,
                        txCount: rawTokenInfo[i].txCount,
                        totalLiquidity: rawTokenInfo[i].totalLiquidity,
                        totalVolumeUSD: rawTokenInfo[i].untrackedVolumeUSD,
                        totalFeesGeneratedUSD: rawTokenInfo[i].untrackedVolumeUSD * FEE
                    });
                }
                i++;
            }
        }

        return tokenInfo;
    })
}


export async function getExplodedTokenInfoByBlocks({tokenAddress, blocks, sushiswap = false}: {tokenAddress: string, blocks: Block[], sushiswap?: boolean}){
    let tokenInfo = await getTokenInfoByBlocks({tokenAddress, blocks, sushiswap});

    return {
        prices: tokenInfo.map(info => ({ unixTimestamp: info.unixTimestamp, value: info.priceUSD })),
        totalVolumeUSDValues: tokenInfo.map(info => ({ unixTimestamp: info.unixTimestamp, value: info.totalVolumeUSD })),
        totalFeesGeneratedUSDValues: tokenInfo.map(info => ({ unixTimestamp: info.unixTimestamp, value: info.totalFeesGeneratedUSD })),
        totalTradeCounts: tokenInfo.map(info => ({ unixTimestamp: info.unixTimestamp, value: info.txCount}))
    };
}