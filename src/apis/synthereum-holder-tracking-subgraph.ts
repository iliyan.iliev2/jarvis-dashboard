import { cleanAddressAndRemoveChecksum } from "../utils/web3";
import { pageResults, SingleQueryDescription } from "./the-graph-utils/page-results";

const THE_GRAPH_SYNTHEREUM_URL = 'https://api.thegraph.com/subgraphs/name/aurelienzintzmeyer/synthereum-holder-tracking';


export async function getTokenHolderCounts(tokenAddresses: string[]): Promise<{[key in string]: number}> {
        
    return pageResults({
        timeout: 10e3,
        api: THE_GRAPH_SYNTHEREUM_URL,
        queryBlocks: Object.fromEntries(
            tokenAddresses.map(tokenAddress => {
                let query: SingleQueryDescription = {
                    singleEntity: 'erc20',
                    selection: {
                        id: tokenAddress
                    },
                    shallowProps: ['holders']
                };
                return ['token' + cleanAddressAndRemoveChecksum(tokenAddress), query]
            })
        )
            
    } as const)
    .then(r => {

        let entries = Object.entries(r);
        return Object.fromEntries(
            entries.map(([tokenId, data]) => {
                return [
                    tokenId.split('token')[1],
                    Number(Object(data).holders)
                ]
            })
        );

    });
}  

