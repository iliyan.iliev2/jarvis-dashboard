import dayjs, { OpUnitType } from 'dayjs'
import utc from 'dayjs/plugin/utc'
import deepEqual from 'deep-equal';

dayjs.extend(utc);

export enum TimeWindow {
	//0 
	//1
	DAY = 'DAY',
	WEEK = 'WEEK',
	MONTH = 'MONTH',
	//5
	YEAR = 'YEAR',
	//7
	ALL_TIME = 'ALL_TIME'
}

export const TIME_WINDOWS = Object.keys(TimeWindow) as TimeWindow[];

export enum SecondsTimePeriod {
	TenMinutes = 600,
	HalfAnHour = 1800,
	OneHour = 3600,
	TwelveHours = 3600 * 12
}

export type USDPrice = {
    unixTimestamp: number,
    priceUSD: number
}


export type ETHPrice = {
    unixTimestamp: number,
    derivedETH: number
}


export type TimestampedValue = {
    unixTimestamp: number,
    value: number | bigint,
	numberType?: NumberType
}

export const sortByTimestamp = (a: TimestampedValue, b: TimestampedValue) => (a.unixTimestamp - b.unixTimestamp);

export type Values<T> = T[keyof T]
export type RecReadonly<T> = T extends { } ? {
    readonly [P in keyof T]: RecReadonly<T[P]>;
} : T;
export type PromiseResultType<F extends (...arg: any[]) => Promise<any>> = 
F extends (...arg: any[]) => Promise<infer U> ? U : never

export function computeTimeframe(time_window: TimeWindow) {
	const endTime = dayjs.utc();

	let startTime: number;

	switch (time_window) {
	  case TimeWindow.WEEK:
		startTime = endTime.subtract(1, 'week').endOf('day').unix() - 1;
		break;
	  case TimeWindow.MONTH:
		startTime = endTime.subtract(1, 'month').endOf('day').unix() - 1;
		break;
	  case TimeWindow.ALL_TIME:
		startTime = endTime.subtract(1, 'year').endOf('day').unix() - 1;
		break;
	  default:
		startTime = endTime.subtract(1, 'year').startOf('year').unix() - 1;
		break;
	}
	return startTime;
}

export function formatDate(date: Date, time_window: TimeWindow, opts: {weekday?: boolean, month?: "2-digit" | "long", day?: "2-digit" | "numeric", hour?: "numeric" | "2-digit"}){
	return date.toLocaleString('en', {
		year: (time_window > TimeWindow.WEEK) ? "numeric" : undefined,
		month: (opts.month === undefined && time_window >= TimeWindow.DAY) ? "long" : opts.month,
		day: (opts.day === undefined && time_window >= TimeWindow.DAY) ? "numeric" : opts.day,
		weekday: (time_window <= TimeWindow.MONTH && opts.weekday) ? "long" : undefined,
		hour: (time_window <= TimeWindow.DAY || opts.hour) ? "numeric" : opts.hour,
		hour12: true,
		minute: (time_window <= TimeWindow.DAY || opts.hour) ? "numeric" : opts.hour,
	});
}

export function unixTimestampToDate(unixTimestamp: number){
	return new Date(unixTimestamp * 1000);
}

export function utcUTimestamp(){
	return dayjs().utc().unix();
}

export function utcUTimestampXSecondsAgo(seconds: number){
	return dayjs().utc().subtract(seconds, 'second').unix();
}

export function utcUTimestampXHoursAgo(hours: number){
	return dayjs().utc().subtract(hours, 'hour').unix();
}


export function makeUTimestampRange(arg: {from_seconds_ago: number} | {from_hours_ago: number}): [number, number]{
	return ('from_seconds_ago' in arg) ?
		[utcUTimestampXSecondsAgo(arg.from_seconds_ago), utcUTimestamp()]
		: [utcUTimestampXHoursAgo(arg.from_hours_ago), utcUTimestamp()]
}

/**
 * @returns - an array of timestamps starting from <arg.start>
 * 			  until current UTC (interval between blocks is <arg.interval_in_seconds>)
 */
export function makeTimestamps({start, interval_in_seconds = 3600}: {start: number, interval_in_seconds: number}){
	const endTime = dayjs.utc();
	let time = start;

    const timestamps: number[] = [];
	while (time < endTime.unix()) {
	  timestamps.push(time);
	  time += interval_in_seconds;
	}
  
	// if invalid timestamp format
	if (timestamps.length === 0) {
	  	return [] as number[];
	}
	return timestamps;
}

export function getSecondsTimePeriodForWindow(time_window: TimeWindow): SecondsTimePeriod {
	switch(time_window){
		case TimeWindow.DAY: 
			return SecondsTimePeriod.OneHour;
		case TimeWindow.WEEK: 
		case TimeWindow.MONTH: 
			return SecondsTimePeriod.TenMinutes;
		case TimeWindow.YEAR: 
		case TimeWindow.ALL_TIME: 
			return SecondsTimePeriod.TwelveHours
	}
}

export function makeTimestampsForWindow({time_window}: {time_window: TimeWindow}){

	let interval_in_seconds = getSecondsTimePeriodForWindow(time_window);

	const currentTime = dayjs.utc();
    const windowSize: OpUnitType = 
		time_window === TimeWindow.DAY ? 'day'
		: time_window === TimeWindow.WEEK ? 'week'
		: time_window === TimeWindow.MONTH ? 'month'
		: time_window === TimeWindow.YEAR ? 'year' : 'year';

    const start =
		(time_window === TimeWindow.ALL_TIME) ? 
			1580000000 
			: currentTime.subtract(1, windowSize).startOf('hour').unix();
	return makeTimestamps({
		start,
		interval_in_seconds
	});
}

export function isDefined<T>(arg: T): arg is NonNullable<T>{
	return arg !== undefined && arg !== null;
}


export function removeUndefinedProperties<O extends {[key in string]: any}>(arg: O): O {
	return Object.fromEntries(
		Object.entries(arg).map(([k, v]) => (v !== undefined) ? [k, v] : undefined)
		.filter(isDefined)
	);
}

export function splitArrayIntoSubArrays<T>(array: T[], subarray_min_size: number): T[][]{
	const subarrays: T[][] = [];
	let currentSubArray: T[] = [];

	for (let i = 0; i < array.length; i++) {
		if(i !== 0 && ((i % subarray_min_size) === 0)){
			subarrays.push(currentSubArray);
			currentSubArray = [];
		}
		currentSubArray.push(array[i]);
	}
	if(currentSubArray.length !== 0) {
		subarrays.push(currentSubArray);
	}
	return subarrays;
}

export function splitAsyncOperation<T extends any>(count: number, splittedArg: any[], fn:  (...arg: any[]) => Promise<T[]>): Promise<T[]> {
	return Promise.all(
		splitArrayIntoSubArrays(splittedArg, count)
		.map(e => fn(e))
	).then(r => r.flat());
}

export function coMap<A, B, Fn extends (e1: A, e2: B) => any>(array1: A[], array2: B[], mapFn: Fn){
	let minLength = Math.min(array1.length, array2.length);

	let result: ReturnType<Fn>[] = [];
	for (let i = 0; i < minLength; i++) {
		result.push(mapFn(array1[i], array2[i]))
	}
	return result;
}

export function invertDimension<A>(arrays: A[][]): A[][]{
	if(arrays.length === 0) return [];
	let result: A[][] = [];
	for(let i = 0; i < arrays[0].length; i++){
		result.push( arrays.map(array => array[i])  )
	}
	return result;
}

/**
 * Replace sequences of zeroes (at start) in arrays with the corresponding values (same index) of the completed array
 * If there is no completed array, the starting 0 values are replaced with the fast non zero values in their corresponding
 * array.
 */
export function completeEachOtherStart(arrays: TimestampedValue[][]){
	if(arrays.length === 1) return;
	{
		let length = arrays[0].length;
		if(! arrays.every(array => array.length === length)){
			throw new Error(`completeEachOtherStart : arrays should have the same length`)
		}
	}
	let alreadyCompleteArray = arrays.find(array => array[0].value !== 0);
	if(alreadyCompleteArray === undefined){

		//`No array is completed, first value of arrays: ` + arrays.map(array => array[0].value).join(', ');

		for(let array of arrays){
			let firstNonZeroValue = array.find(e => e.value !== 0);
			if(firstNonZeroValue === undefined){
				return;
			}
			let i = 0;
			while(array.length > i && array[i].value === 0){
				array[i].value = firstNonZeroValue.value;
				i++;
			}
		}
		return;
	}

	for(let array of arrays){
		if(array !== alreadyCompleteArray){
			let i = 0;
			while(array[i].value === 0 && array.length > i){
				array[i].value = alreadyCompleteArray[i].value;
				i++;
			}
		}
	}
}

export function sum(array: number[]): number {
	return array.reduce((total, e) => total + e, 0);
}

export function clamp(n: number, min: number, max: number){
	if(min > max){
		throw new Error(`Min value cannot be greater than max value`);
	}
	if(n < min) return min;
	if(n > max) return max;
	return n;
}


export type NumberType = 'priceUSD' | 'valueUSD' | 'marketcap' | 'simple-format' | 'unformatted' | 'percentage';



export const formatNumberValue = (value: number, type?: NumberType, {compactIfHigh = false}: {compactIfHigh?: boolean} = {}) => {
    
	if(compactIfHigh === true && value >= 10_000){
		const symbols = ['K', 'M', 'B']
		let i = -1;
		while(value >= 10_000 && i < symbols.length - 1){
			value /= 1000;
			i++;
		}
		return (String(type).includes('USD') ?  '$' : '') + value + symbols[i];
	}

	switch (type) {
        case 'marketcap': return Number(value).toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
            maximumFractionDigits: 0
		});
		case 'priceUSD': return Number(value).toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
			minimumFractionDigits: 2,
			maximumFractionDigits: 6,
        });
		case 'valueUSD': return Number(value).toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
			minimumFractionDigits: 0,
			maximumFractionDigits: Math.max(0, 3 - Math.floor(Math.log10(Math.max(value, Number.EPSILON)))),
        });
		case 'unformatted': return value;
		case 'percentage': return value.toPrecision(2) + '%';
		case 'simple-format': default: return value.toLocaleString('en-US');
	}
};
export function getNumberTypeUnit(typ: NumberType){
	switch(typ){
		case 'marketcap': case 'priceUSD': case 'valueUSD': return 'USD';
		case 'percentage': return '%';
		default: return undefined;
	}
}

export function waitFor(duration: number): Promise<void> {
	return new Promise(((resolve, reject) => {
		setTimeout(resolve, duration);
	}));
}



const DEBOUNCE_PERIOD = 1000;
const NO_ENTRY = Symbol();

export function debounceAsync<
	R, 
	Fn extends (...arg: any[]) => Promise<R>
>(fn: Fn): Fn {

	let prevCallArgList: Parameters<Fn>[] = [];
	let prevCallArgsToTimestamp = new Map<Parameters<Fn>, number>();
	let callArgsToPromise = new Map<Parameters<Fn>, Promise<R>>();

	return async function(...args: Parameters<Fn>): Promise<R> {

		let equalPrevCallArgs = prevCallArgList.find(prevCallArgs => deepEqual(prevCallArgs, args));

		if(equalPrevCallArgs === undefined){
			prevCallArgList.push(args);
			equalPrevCallArgs = args;
		}

		let prevCallTimestamp = prevCallArgsToTimestamp.get(equalPrevCallArgs) || - Infinity;

		let currentTimestamp = Date.now();
		let resultPromise = 
			callArgsToPromise.has(equalPrevCallArgs) ? 
			(callArgsToPromise.get(equalPrevCallArgs) as Promise<R>) 
			: NO_ENTRY;
		if(
			resultPromise === NO_ENTRY
			|| (currentTimestamp - prevCallTimestamp) > DEBOUNCE_PERIOD
		){
			prevCallArgsToTimestamp.set(equalPrevCallArgs, currentTimestamp);
			resultPromise = new Promise<R>(async (resolve, reject) => {
				fn(...args).then(r => resolve(r)).catch(err => reject(err));
			});
			callArgsToPromise.set(equalPrevCallArgs, resultPromise);
			return await resultPromise;
		} 
		
		//console.debug('(debounce) ignored call ' + fn.name)
		let result = await resultPromise;
		//console.log('(debounce) result = ', result)
		return result;
	} as Fn;
}


export function absDistance(value1: number, value2: number){
	return Math.abs(value1 - value2)
}

export function lastOrDefault<T>(array: T[], defaultValue: T): T {
	return array[array.length - 1] ?? defaultValue;
}

export function capitalize(s: string){
	return s[0].toUpperCase() + s.slice(1);
}

export function removeDashs(s: string){
	return s.replaceAll('-', ' ');
}

export function takeOneElemIn<T>(array: T[], n: number): T[] {
	let result: T[] = [];
	for(let i = 0; i < array.length; i++){
		if(i % n === 0){
			result.push(array[i])
		}
	}
	return result;
}