import React from 'react';
import { Provider as StateProvider } from 'react-redux';
import './App.css';
import { AppThemeProvider } from './components/AppThemeProvider';
import { ContentRoot } from './components/ContentRoot';
import store from './state/store';

function App() {  
  return (
    <div className="App">
      <StateProvider store={store}>
        {/* <header className="App-header">
        </header> */}

        {/* <div>
          <AreaChart />
        </div> */}

        <AppThemeProvider>
          <ContentRoot></ContentRoot>
        </AppThemeProvider>

      </StateProvider>

    </div>
  );
}

export default App;
